<div class="left_content">
	<div class="crumb_nav">
		<?php echo anchor('http://localhost/online/index.php/main/product/' . $product["cat_id"], "Products");?>
		&gt;&gt; <?php echo $product["product_name"]
		?>
		&gt;&gt; Details
	</div>
	<div class="title">
		<span class="title_icon"><img src="images/bullet1.gif" alt="" title="" /></span><?php echo "Product Name: " . $product["product_name"];?>
	</div>
	<div class="feat_prod_box_details">
		<div class="prod_img">
			<a href="#"><img src="<?php echo $product['image'];?>" alt="" title="" border="0" /></a>
			<br />
			<br />
			<a href="<?php echo $product['image'];?>" rel="lightbox"><img src="http://localhost/online/css/images/zoom.gif" alt="" title="" border="0" /></a>
		</div>
		<div class="prod_det_box">
			<div class="box_top"></div>
			<div class="box_center">
				<div class="prod_title">
					Details
				</div>
				<p class="details">
					<?php echo "Description : " . $product['description'];
					echo "<br/> Product ID  : " . $product['product_id'];
					echo "<br/> Stock : " . $product['stock'];
					echo "<br/> Promotion : " . $product['promotion'] . " % ";
					?>
				</p>
				<div class="price">
					<strong>PRICE:</strong><span class="red"><?php echo $product['selling_price'] . " BDT";?></span>
				</div>
				<div class="price">
					<strong>COLORS:</strong>
					<span class="colors"><img src="http://localhost/online/css/images/color1.gif" alt="" title="" border="0" /></span>
					<span class="colors"><img src="http://localhost/online/css/images/color2.gif" alt="" title="" border="0" /></span>
					<span class="colors"><img src="http://localhost/online/css/images/color3.gif" alt="" title="" border="0" /></span>
				</div>
				<?php
if ($this -> session -> userdata('is_loged_in')) {
	
echo form_open("http://localhost/online/index.php/customer/addcart/".$product['product_id']);
	
echo "<br/><br/> Please insert the quantity : <input type='text' size='2' name = 'quantity'></input>";
echo "<div class='form_row'>
<input type='submit' class='contact' value='Add to cart' />
</div>";

echo form_close();
} else {
echo "<br/><br/>Please " . anchor('http://localhost/online/index.php/main/login', "Login ") . "Or  " . anchor('http://localhost/online/index.php/main/showform', "Register ") . "to Purchase .";
}
				?>	<!-- <a href="#" class="more"><img src="http://localhost/online/css/images/order_now.gif" alt="" title="" border="0" /></a> -->
				<div class="clear"></div>
			</div>
			<div class="box_bottom"></div>
		</div>
		<div class="clear"></div>
	</div>
</div>