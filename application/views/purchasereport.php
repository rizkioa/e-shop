<div class="left_content">
	<div class="title">
		<span class="title_icon"><img src="" alt="" title="" /></span>You Have Purchased These Products 
	</div>
	<div class="feat_prod_box_details">
		<table class="cart_table">
			<tr class="cart_title">
				<td>Product ID</td>
				<td>Product name</td>
				<td>Unit price</td>
				<td>Quantity</td>
				<td>Discount</td>
				<td>Total</td>
			
			</tr>
			<?php
			foreach ($cartinfo as $product) {
				echo "	<tr>";
				echo "	<td>" . $product -> product_id . "</td>";
				echo "	<td>" . $product -> product_name . "</td>";
				echo "	<td>" . $product -> unit_price . "</td>";
				echo "	<td>" . $product -> quantity . "</td>";
				echo "	<td>" . $product -> discount . "</td>";
				echo "	<td>" . $product -> total . "</td>";
				
				echo "  </tr>";

			}
			?>

			<tr>
				<td colspan="4" class="cart_total"><span class="red">TOTAL PRODUCT:</span></td>
				<td> <?php echo $price["quantity"];?></td>
			</tr>
			<tr>
				<td colspan="4" class="cart_total"><span class="red">TOTAL:</span></td>
				<td> <?php echo $price["total"]." BDT";?></td>
			</tr>
		</table>
		
	</div>
	<div class="clear"></div>
</div>