<?php $this -> load -> model('edata');
$this -> load -> helper('form');
$this -> load -> library('session');
$this -> load -> helper('url');

echo "<hr><p><font size='12' color='blue' face='arial'>Product Wise Sales Report</font></p> <br/><hr>";
?>

<div id="infobox">
	<h3>Product Sales Report </h3>
	<table>
		<thead>
			<tr>
				<th>Product ID </th>
				<th>Product Name</th>
				<th>Catagory ID</th>
				<th>Date Added</th>
				<th>Promotion Amount</th>
				<th>Promotion From</th>
				<th>Promotion To</th>
				<th>Promotion Active</th>
				<th>Average Bought Price</th>
				<th>Selling Price</th>
				<th>Stock</th>
			
				<th>Total Sale</th>
				<th>Details</th>
			</tr>
		</thead>
		<tbody>
			<?php

			foreach ($report as $product) {
				echo "	<tr>";
				echo "<td><a href='#'>" . $product -> product_id . "</a></td>";
				echo "<td>" . $product -> product_name . "</td>";
				echo "<td>" . $product -> cat_id . "</td>";
				echo "<td>" . $product -> date_added . "</td>";
				echo "<td>" . $product -> promotion . "</td>";
				echo "<td>" . $product -> promo_from . "</td>";
				echo "<td>" . $product -> promo_to . "</td>";
				echo "<td>" . $product -> promo_active . "</td>";
				echo "<td>" . $product -> avg_bought_price . "</td>";
				echo "<td>" . $product -> selling_price . "</td>";
				echo "<td>" . $product -> stock . "</td>";
			//	echo "<td>" . $product -> threshold . "</td>";
				echo "<td>" . $product -> num_sale . "</td>";
				echo "<td>" .anchor("http://localhost/online/index.php/admin/productdetails/".$product -> product_id,"Details"). "</td>";
				echo "</tr>";

			}
			echo $this -> pagination -> create_links();
			?>
		</tbody>
	</table>
</div>