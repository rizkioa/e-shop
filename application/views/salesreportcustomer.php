<?php 
		$this -> load -> model('edata');
		$this -> load -> helper('form');
		$this -> load -> library('session');
		$this -> load -> helper('url');
		
		
echo "<hr><p><font size='12' color='blue' face='arial'>Customer Wise Sales Report </font></p> <br/><hr>";

?>

	<div id="infobox">
		<h3>Customer Sales Report</h3>
		<table>
			<thead>
				<tr>
					<th>Customer ID </th>
					<th>Name</th>
					<th>E-mail</th>
					<th>Registration Date</th>
					<th>Total Cash in Account</th>
					<th>Number Of Order</th>
					<th>Amount Of Purchase</th>
					<th>Total Product Purchased</th>
					<th>More Details</th>
				</tr>
			</thead>
			<tbody>
		<?php 		
			

			
				foreach ($report as $customer) {
					echo "	<tr>";
					echo "<td><a href='#'>" .$customer->customer_id."</a></td>";
					echo "<td>". $customer->name . "</td>";
					echo "<td>". $customer->email . "</td>";
					echo "<td>". $customer->date . "</td>";
					echo "<td>". $customer->total_credit . "</td>";
					echo "<td>". $customer->num_of_purchase . "</td>";
					echo "<td>". $customer->purchase_amount . "</td>";
					echo "<td>". $customer->total_product_purchased . "</td>";
					echo "<td>".anchor("http://localhost/online/index.php/admin/detailscustomer/".$customer->customer_id,"Details")."</td>";
					echo "</tr>";
					
				}
				echo $this->pagination->create_links();
				?>  
			</tbody>
			
		</table>
		
			

	</div>