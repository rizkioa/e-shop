<div class="title">
	<span class="title_icon"><img src="images/bullet1.gif" alt="" title="" /></span>Register
</div>
<div class="feat_prod_box_details">
	<p class="details">
		All fields are required...
	</p>
	<div class="contact_form">
		<div class="form_subtitle">
			Create new account
		</div>
		 <?php echo form_open('http://localhost/online/index.php/main/customerinsert');?>    
			<div class="form_row">
				<label class="contact"><strong>Name:</strong></label>
				<input type="text" class="contact_input" name="name"/>
			</div>
			
			<div class="form_row">
				<label class="contact"><strong>Email:</strong></label>
				<input type="text" class="contact_input" name="email"/>
			</div>
			<div class="form_row">
				<label class="contact"><strong>Password:</strong></label>
				<input type="password" class="contact_input" name="password"/>
			</div>
			<div class="form_row">
				<label class="contact"><strong>Phone:</strong></label>
				<input type="text" class="contact_input" name="contact_no"/>
			</div>
			<div class="form_row">
				<label class="contact"><strong>Country:</strong></label>
				<input type="text" class="contact_input" name="country"/>
			</div>
			<div class="form_row">
				<label class="contact"><strong>State/Div:</strong></label>
				<input type="text" class="contact_input" name="state"/>
			</div>
			<div class="form_row">
				<label class="contact"><strong>Address:</strong></label>
				<input type="text" class="contact_input" name="add"/>
			</div>
			<div class="form_row">
				<div class="terms">
					<input type="checkbox" name="terms" />
					I agree to the <a href="#">terms &amp; conditions</a>
				</div>
			</div>
			<div class="form_row">
				<input type="submit" class="contact" value="register" />
			</div>
		 <?php form_close(); ?>
	</div>
</div>