<div id="infowrap">
	<div id="infobox">
		<h3>Inventory</h3>
		<table>
			<thead>
				<tr>
					<th>Product ID</th>
					<th>avg_bought_price</th>
					<th>Selling price</th>
					<th>Threshold Level</th>
					<th>Stock</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><a href="#">1322</a></td>
					<td>145</td>
					<td>454</td>
					<td>54</td>
					<td>454</td>
					<td><a href="">buy more</a></td>
				</tr>
				<tr>
					<td><a href="#">1212</a></td>
					<td>100</td>
					<td>454</td>
					<td>54</td>
					<td>454</td>
					<td><a href="">buy more</a></td>
				</tr>
				<tr>
					<td><a href="#">1212</a></td>
					<td>100</td>
					<td>454</td>
					<td>54</td>
					<td>454</td>
					<td><a href="">buy more</a></td>
				</tr>
			</tbody>
		</table>
	</div>
	<div id="infobox">
		<h3>Promotion</h3>
		<table>
			<thead>
				<tr>
					<th>Product ID</th>
					<th>From</th>
					<th>To</th>
					<th>Promotion</th>
					
					
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><a href="#">1322</a></td>
					<td>145</td>
					<td>454</td>
					<td>54</td>
					
					
				</tr>
				<tr>
					<td><a href="#">1212</a></td>
					<td>100</td>
					<td>454</td>
					<td>54</td>
					
					
				</tr>
				<tr>
					<td><a href="#">1212</a></td>
					<td>100</td>
					<td>454</td>
					<td>54</td>
					
					
				</tr>
			</tbody>
		</table>
	</div>
</div>