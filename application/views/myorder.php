<div class="left_content">
<div class="title">
	<span class="title_icon"><img src="" alt="" title="" /> My Order Details </span>
</div>
<div class="feat_prod_box_details">
	<table class="cart_table">
			<tr class="cart_title">
	
			<th scope="col">Purchase ID </th>
			<th scope="col">Delivery Division</th>
			<th scope="col">Delivery Address</th>
			<th scope="col">Date Of Purchase</th>
			<th scope="col">Shipment Charge</th>
			<th scope="col">Total Product</th>
			<th scope="col">Total Amount</th>
			<th scope="col">Voucher </th>
			<th scope="col">Action </th>
		</tr>

	<tbody>
		<?php
		foreach ($order as $order) {
			echo "<tr>";

			echo "<td>" . $order -> purchase_id . "</td>";
			echo "<td>" . $order -> delivery_div . "</td>";
			echo "<td>" . $order -> delivery_address . "</td>";
			echo "<td>" . $order -> date . "</td>";
			echo "<td>" . $order -> shipment_charge . "</td>";
			echo "<td>" . $order -> total_quantity . "</td>";
			echo "<td>" . $order -> sales . "</td>";
			echo "<td>" . anchor("http://localhost/online/index.php/customer/myvoucher/".$order -> purchase_id,"See Voucher") . "</td>";
			if ($order -> date >= date("Y-m-d")) { 	echo "<td>" . anchor("http://localhost/online/index.php/customer/deleteorder/".$order -> purchase_id,"Cancel") . "</td>";
			}
			echo "</tr>";

		}
		?>
	</tbody>
</table>
</div>
</div>