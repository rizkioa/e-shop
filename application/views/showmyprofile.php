<br /><div class="title">
		<span class="title_icon"><img src="" alt="" title="" /> 
			My Profile
		</span>
	</div>

<table id="ver-minimalist" summary="Delivery Areas">
	<thead>
		<tr>
			<th scope="col">Information</th>
			<th scope="col">Result</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Date Of Registration </td> <td><?php echo $customer["date"]?></td>
		</tr>
		<tr>
			<td>Customer ID</td> <td><?php echo $customer["customer_id"]?></td>
		</tr>
		<tr>
			<td> Name </td><td><?php echo $customer["name"]?></td>
		</tr>
		<tr>
			<td>Country </td><td><?php echo $customer["country"]?></td>
		</tr>
		<tr>
			<td>State/Division </td><td><?php echo $customer["state"]?></td>
		</tr>
		<tr>
			<td>Full Address</td><td><?php echo $customer["address"]?></td>
		</tr>
		<tr>
			<td>Contact Number </td><td><?php echo $customer["contact_no"]?></td>
		</tr>
		<tr>
			<td>E-mail Id </td><td><?php echo $customer["email"]?></td>
		</tr>
		<tr>
			<td>Account Number </td><td><?php echo $customer["account_num"]?></td>
		</tr>
		<tr>
			<td>Total Cash in Account</td><td><?php echo $customer["total_credit"] . " BDT";?></td>
		</tr>
		<tr>
			<td>Number of Order</td><td><?php echo $customer["num_of_purchase"]?></td>
		</tr>
		<tr>
			<td>Number Of Product Purchased</td><td><?php echo $customer["total_product_purchased"]?></td>
		</tr>
	</tbody>
</table>