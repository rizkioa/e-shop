<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<title>eShop</title>
	<link rel="stylesheet" href="http://localhost/online/css/style.css" type="text/css" media="all" />
	<link rel="stylesheet" href="http://localhost/online/css/style2.css" type="text/css" media="all" />
	<!--[if lte IE 6]><link rel="stylesheet" href="http://localhost/online/css/ie6.css" type="text/css" media="all" /><![endif]-->
	
	<meta name="keywwords" content="Shop Around - Great free html template for on-line shop. Use it as a start point for your on line business. The template can be easily implemented in many open source E-commerce platforms" />
	<meta name="description" content="Shop Around - Great free html template for on-line shop. Use it as a start point for your on line business. The template can be easily implemented in many open source E-commerce platforms" />
	
	<!-- JS -->
	<script src="http://localhost/online/js/jquery-1.4.1.min.js" type="text/javascript"></script>	
	<script src="http://localhost/online/js/jquery.jcarousel.pack.js" type="text/javascript"></script>	
	<script src="http://localhost/online/js/jquery-func.js" type="text/javascript"></script>	
	<!-- End JS -->
	
</head>
<body>
	
<!-- Shell -->	
<div class="shell">

	<!-- Header -->	
	<div id="header">
		<h1 id="logo"><a href="#">eShop</a></h1>	
		
		<!-- Cart -->
		<div id="cart">
		<?php 
		echo $cart;
		?>
		</div>
		<!-- End Cart -->
		
		<!-- Navigation -->
		<?php 
		$this->load->view('navigation');
		?>
		<!-- End Navigation -->
	</div>
	<!-- End Header -->
	
	<!-- Main -->
	<div id="main">
		<div class="cl">&nbsp;</div>
		
		<!-- Content -->
		<div id="content">
			<!-- Main content -->
			<!-- Content Slider ar onno kichu thakle main e dite hobe  -->
				<?php 
				echo  $main ;
				?>
		<!-- End main Content Slider -->
			
			<!--  eita body hishabe thakbe ...Products  er kichu sample thakbe eikhane -->
			<?php 
			 echo $body;
			?>
			<!-- End Products -->
			
		</div>
		<!-- End Content -->
		
		<!-- Sidebar -->
		<div id="sidebar">
			
			<!-- Search -->
			<?php 
			echo $sidebar;
			?>
			<!-- End Search -->
			
			<!-- Categories or options depends on user loged in ...-->
			<div class="box categories">
				<?php 
				echo $menu;
				?>
			</div>
			<!-- End Categories or options-->
			
		</div>
		<!-- End Sidebar -->
		
		<div class="cl">&nbsp;</div>
	</div>
	<!-- End Main -->
	
	<!-- Side Full -->
	<div class="side-full">
		
		<!-- Side niche full view  ....More Products -->
		<?php 
		echo $sidefull;
		?>
		<!-- End More Products -->
		
		<!-- Text Cols -->
		<?php 
		echo $last;
		?>
		<!-- End Text Cols -->
		
	</div>
	<!-- End Side Full -->
	
	<!-- Footer -->
	<div id="footer">
		<?php 
		echo $foot;
		?>
		<p class="right">
			&copy; eShop.com
			Developped <a href="" target="_blank" title="The Sweetest CSS Templates WorldWide">by Umama Tasnim and Rifatul Islam</a>
		</p>
	</div>
	<!-- End Footer -->
	
</div>	
<!-- End Shell -->
	
	
</body>
</html>