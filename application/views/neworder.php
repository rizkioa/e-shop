<div id="infowrap">
	<div id="infobox">
		<h3>Latest Order </h3>
		<table>
			<thead>
				<tr>
					<th>Purchase ID </th>
					<th>Customer ID </th>
					<th>Date</th>
					<th>Country</th>
					<th>Address</th>
					<th>Customer Acount Number</th>
					<th>Shipment Charge</th>
					<th>Items</th>
					<th>Grand Total(Tk.)</th>
					<th>Sum Of Avg Bought Price</th>
					<th>Profit</th>
					<th>Details</th>
					<th>Edit Order</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$this -> load -> model('edata');

				$data = $this -> edata -> getlast5order();
				foreach ($data as $order) {
					echo '<tr>';
					echo '<td>' . $order -> purchase_id . '</td>';
					echo '<td><a href="#">' . $order -> customer_id . '</a></td>';
					echo '<td>' . $order -> date . '</td>';
					echo '<td>' . $order -> delivery_div . '</td>';
					echo '<td>' . $order -> delivery_address . '</td>';
					echo '<td>' . $order -> pin . '</td>';
					echo '<td>' . $order -> shipment_charge . '</td>';
					echo '<td>' . $order -> total_quantity . '</td>';
					echo '<td>' . $order -> sales . '</td>';
					echo '<td>' . $order -> sum_avg_b_price . '</td>';
					echo '<td>' . $order -> profit . '</td>';
					echo '<td>' . anchor("http://localhost/online/index.php/admin/detailsorder/".$order -> purchase_id,"Details") . '</td>';
					echo '<td>' . anchor("http://localhost/online/index.php/admin/showeditorder/".$order -> purchase_id,"Edit")  . '</td>';
					echo '</tr>';

				}
				?>
			</tbody>
		</table>
	</div>
</div>