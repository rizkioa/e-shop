<?php
//info box to show admin some overviw or report of the shop..
?>

<div id="infowrap">
	<div id="infobox">
		<h3>Last 5 Orders</h3>
		<table>
			<thead>
				<tr>
					<th>Purchase ID </th>
					<th>Customer ID </th>
					<th>Date</th>
					<th>Items</th>
					<th>Grand Total(Tk.)</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$this -> load -> model('edata');

				$data = $this -> edata -> getlast5order();
				foreach ($data as $order) {
					echo '<tr>';
					echo '<td>' . $order -> purchase_id . '</td>';
					echo '<td><a href="#">' . $order -> customer_id . '</a></td>';
					echo '<td>' . $order -> date . '</td>';
					echo '<td>' . $order -> total_quantity . '</td>';
					echo '<td>' . $order -> sales . '</td>';
					echo '</tr>';

				}
				?>
			</tbody>
		</table>
	</div>
		<div id="infobox">
		<h3>5 Bestseller Product</h3>
		<table>
			<thead>
				<tr>
					<th>Prodcut</th>
					<th>Average Bought Price</th>
					<th>Selling Price</th>
					<th>Stock</th>
					<th>Number of Sell</th>
					<th>Total Sale</th>
				
				</tr>
			</thead>
			<tbody>
				<?php 
				$data = $this->edata->bestsell();
				foreach($data as $product){
					echo "	<tr>";
					echo "<td>".anchor("#",$product->product_name)."</td>";
					echo "	<td>".$product->avg_bought_price."</td>";
					echo "	<td>".$product->selling_price."</td>";
					echo "	<td>".$product->stock."</td>";
					echo "	<td>".$product->num_sale."</td>";
					echo "	<td>".$product->sale."</td>";
					echo "</tr>";
					
				}
				?>
				
			
				
			</tbody>
		</table>
	</div>
	<div id="infobox">
		<h3>Shop Overview</h3>
		<table>
			<thead>
				<tr>
					<th>Total Sales(Tk)</th>
					<th>Total Profit(Tk)</th>
					<th>Total Order</th>
					<th>Total Customer</th>
					<th>Total Product</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$this -> load -> model('edata');

				$data = $this -> edata -> overview();

				foreach ($data['sale'] as $sales) {
					echo '<tr>';
					echo '<td>' . $sales -> total . '</td>';

				}
				foreach ($data['profit'] as $profit) {

					echo '<td>' . $profit -> profit . '</td>';

				}
				foreach ($data['order'] as $order) {

					echo '<td>' . $order -> order_no . '</td>';

				}
				foreach ($data['customer'] as $customer) {

					echo '<td>' . $customer -> customer . '</td>';

				}
				foreach ($data['product'] as $product) {

					echo '<td>' . $product -> product_no . '</td>';

				}
				echo '</tr>';
				?>
			</tbody>
		</table>
	</div>

	<div id="infobox">
		<h3>Stock Overview</h3>
		<table>
			<thead>
				<tr>
					<th>Product ID</th>
					<th>Threshold Level</th>
					<th>Stock</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<?php 
			$this -> load -> model('edata');

				$data = $this -> edata -> stock();
				foreach ($data as $stock) {
					echo "	<tr>";
					echo "<td><a href='#'>" .$stock->product_id ."</a></td>";
					echo "<td>". $stock->threshold . "</td>";
					echo "<td>". $stock->stock . "</td>";
					echo "<td><a href='#'>add more</a></td>";
					echo "</tr>";
					
				}
				?>
			
			
			
				
			</tbody>
		</table>
	</div>
	<div id="infobox">
		<h3>New Customers</h3>
		<table>
			<thead>
				<tr>
					<th>Customer</th>
					<th>Email</th>
					<th>Orders</th>
					<th>Total Product Purchased</th>
					<th>Total </th>
				</tr>
			</thead>
			<tbody>
				<?php 
				$data = $this->edata->newcustomer();
				foreach($data as $customer){
					echo "	<tr>";
					echo "<td>".anchor("#",$customer->name)."</td>";
					echo "	<td>".$customer->email."</td>";
					echo "	<td>".$customer->num_of_purchase."</td>";
					echo "	<td>".$customer->total_product_purchased."</td>";
					echo "	<td>".$customer->purchase_amount."</td>";
					echo "</tr>";
					
				}
				?>
				
			
				
			</tbody>
		</table>
	</div>
</div>
