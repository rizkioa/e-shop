<?php
/**
 *
 */
class Edata extends CI_Model {

	function __construct() {
		$this -> load -> library('pagination');
		$this -> load -> library('encrypt');
		$this -> load -> library('session');
		$this -> load -> database();
	}

	public function login() {
		$email = $this -> input -> post('email');
		$password = $this -> input -> post('password');
		$this -> load -> database();
		$query = $this -> db -> get_where('customerinfo', array('email' => $email, 'password' => $password));
		if ($query -> num_rows > 0) {
			$user_data = $query -> row_array();
			if ($user_data['email'] != $email || $user_data['password'] != $password)
				return false;
			//Creating a new session...//
			$this -> session -> sess_destroy();
			$this -> session -> sess_create();
			$user_info = array('name' => $user_data['name'], 'email' => $email, 'is_loged_in' => true, 'admin' => $user_data['admin'], 'customer_id' => $user_data['customer_id']);
			$this -> session -> set_userdata($user_info);
			///finished ...///
			return true;
		} else {
			return false;
		}
	}

	// main page database ...//
	public function delivery() {
		$query = $this -> db -> get('d_areas');
		return $query -> result();
	}

	public function catagory() {
		$query = $this -> db -> get_where('catagory', array('parent_cat' => 0));
		return $query -> result();
	}

	public function suggest($data = "") {
		$this -> db -> insert('message', $data);
		return true;

	}

	public function insertcustomer($data = "") {
		$this -> db -> insert('customerinfo', $data);
		return true;

	}

	public function loadcatagory($catid = "") {
		$query = $this -> db -> get_where('catagory', array('parent_cat' => $catid));
		return $query;
	}

	public function parentcatagory($catid = "") {
		$query = $this -> db -> get_where('catagory', array('cat_id' => $catid));
		return $query -> row_array();
	}

	public function loadproduct($id = "") {

		$this -> db -> select(" productinfo.`product_id`, productinfo.`product_name`,productinfo.`image`,`cat_id`, `product_type`,`description`,`date_added`,`promotion`, `promo_from`, `promo_to`, `promo_active` , inventory.avg_bought_price, inventory.`selling_price`, inventory.`stock`,inventory.`threshold`,inventory.`num_sale`");

		$this -> db -> join("inventory", " productinfo.product_id= inventory.product_id ", ' inner ');
		$query = $this -> db -> get_where('productinfo', array('cat_id' => $id));
		return $query -> result();

	}

	public function productdetails($id = "") {

		$this -> db -> select(" productinfo.`product_id`, productinfo.`product_name`,productinfo.`image`,`cat_id`, `product_type`,`description`,`date_added`,`promotion`, `promo_from`, `promo_to`, `promo_active` , inventory.avg_bought_price, inventory.`selling_price`, inventory.`stock`,inventory.`threshold`,inventory.`num_sale`");

		$this -> db -> join("inventory", " productinfo.product_id= inventory.product_id ", ' inner ');
		$query = $this -> db -> get_where('productinfo', array('productinfo.`product_id` ' => $id));
		return $query -> row_array();

	}

	/// END ...///

	////CUSTOMER >>>DATABSE ...

	public function insertcart($data = "") {
		$info = array('customer_id' => $this -> session -> userdata("customer_id"), 'product_id' => $data['product_id']);
		$query = $this -> db -> get_where('cart_table', $info);
		$result = $query -> row_array();
		if ($query -> num_rows() > 0) {

			$pretotal = $result["total"];
			$total = ($data["selling_price"] - $data["selling_price"] * $data["promotion"] / 100) * $this -> input -> post('quantity') + $pretotal;

			$quantity = $this -> input -> post('quantity') + $result["quantity"];
			$memo = $total - $data["avg_bought_price"] * $quantity;
			$update = array('total' => $total, 'quantity' => $quantity, 'memo' => $memo);

			$this -> db -> where('customer_id', $this -> session -> userdata("customer_id"));
			$this -> db -> where('product_id', $data['product_id']);

			$this -> db -> update('cart_table', $update);
		} else {
			$total = ($data["selling_price"] - $data["selling_price"] * $data["promotion"] / 100) * $this -> input -> post('quantity');
			$memo = $total - $data["avg_bought_price"] * $this -> input -> post('quantity');
			$cart = array('customer_id' => $this -> session -> userdata("customer_id"), 'product_id' => $data['product_id'], 'product_name' => $data['product_name'], 'unit_price' => $data['selling_price'], 'avg_bought_price' => $data['avg_bought_price'], 'quantity' => $this -> input -> post('quantity'), 'discount' => $data['promotion'], 'total' => $total, 'memo' => $memo);
			$this -> db -> insert('cart_table', $cart);
		}

	}

	public function getcart() {
		$query = $this -> db -> get_where('cart_table', array('customer_id ' => $this -> session -> userdata("customer_id")));
		return $query -> result();
	}

	public function sumcart() {
		$query = $this -> db -> query("SELECT SUM(`total`) as total , SUM(`quantity`) as quantity FROM cart_table WHERE `customer_id`=" . $this -> session -> userdata("customer_id"));
		return $query -> row_array();
	}

	public function deletecart($id) {
		$info = array('customer_id' => $this -> session -> userdata("customer_id"), 'product_id' => $id);
		$query = $this -> db -> delete('cart_table', $info);

	}

	public function editcart($data = "") {

		$total = ($data["selling_price"] - $data["selling_price"] * $data["promotion"] / 100) * $this -> input -> post('quantity');
		$memo = $total - $data["avg_bought_price"] * $this -> input -> post('quantity');

		$updateinfo = array("quantity" => $this -> input -> post("quantity"), "total" => $total, "memo" => $memo);
		$this -> db -> where("customer_id", $this -> session -> userdata("customer_id"));
		$this -> db -> where("product_id", $data["product_id"]);
		$query = $this -> db -> update('cart_table', $updateinfo);

	}

	public function getareas() {
		$query = $this -> db -> get('d_areas');
		return $query -> result();
	}

	public function getcountry() {
		$query = $this -> db -> query("SELECT DISTINCT `country`  FROM d_areas");
		return $query -> result();
	}

	public function findsum() {
		$info = array('customer_id' => $this -> session -> userdata("customer_id"));
		$query = $this -> db -> get_where('cart_table', $info);
		if ($query -> num_rows() > 0) {
			$product = $query -> result();
			foreach ($product as $product) {
				$query1 = $this -> db -> get_where('inventory', array("product_id" => $product -> product_id));
				$stock = $query1 -> row_array();
				if ($product -> quantity > $stock["stock"]) {
					return array("success" => false, "product" => $product -> product_id);
					break;
				}

			}
			return array("success" => true, "product" => $product -> product_id);
		} else {
			return array("success" => false, "product" => 0);
		}

	}

	public function verifyaccount() {
		$pin = $this -> input -> post("prepaid_pin");
		$query = $this -> db -> get_where('customerinfo', array("customer_id" => $this -> session -> userdata("customer_id"), "account_num" => $pin));
		$customer = $query -> row_array();
		if ($query -> num_rows > 0) {

			$result = $this -> db -> query("SELECT SUM(`total`) as total , SUM(`quantity`) as quantity FROM cart_table WHERE `customer_id`=" . $this -> session -> userdata("customer_id"));
			$cartinfo = $result -> row_array();
			if ($customer["total_credit"] >= $cartinfo["total"]) {
				return array("success" => true, "message" => "", "total_credit" => $customer["total_credit"]);
			} else {
				return array("success" => false, "message" => "You do not have sufficeint credit in your account to purchase.Please refill via prepaid card", "total_credit" => $customer["total_credit"]);
			}
		} else {
			return array("success" => false, "message" => "Your Pin Code Did Not match With the Database . Make sure You have insert correctly");
		}

	}

	public function dopurchase($totalcash = "") {
		$cartsum = $this -> db -> query("SELECT SUM(`total`) as total , SUM(`quantity`) as quantity , SUM(`quantity` * `avg_bought_price`) as avg , SUM(`memo`) as profit FROM cart_table WHERE `customer_id` = " . $this -> session -> userdata("customer_id"));
		$sum = $cartsum -> row_array();
		$pin = $this -> input -> post("prepaid_pin");
		$address = $this -> input -> post("address");
		$country = $this -> input -> post("country");
		$area = $this -> input -> post("area");
		$this -> db -> where("country", '$country');
		$this -> db -> where("areas", '$area');
		$shipmentquery = $this -> db -> get("d_areas");

		$shipment = $shipmentquery -> row_array();
		$purchaseinfo = array("customer_id" => $this -> session -> userdata("customer_id"), "delivery_div" => $country, "delivery_address" => $address, "date" => date("Y-m-d"), "pin" => $pin, "shipment_charge" => 100, "total_quantity" => $sum["quantity"], "sales" => $sum["total"], "sum_avg_b_price" => $sum["avg"], "profit" => $sum["profit"]);
		$this -> db -> insert("purchase", $purchaseinfo);

		//// End inserting purchase ...///

		/// Updating product info ...///
		$getcartquery = $this -> db -> get_where("cart_table", array("customer_id" => $this -> session -> userdata("customer_id")));
		$cartinfo = $getcartquery -> result();

		foreach ($cartinfo as $cartinfo) {
			$productquery = $this -> db -> get_where("inventory", array("product_id" => $cartinfo -> product_id));
			$product = $productquery -> row_array();
			$numsale = $product["num_sale"] + $cartinfo -> quantity;
			$stock = $product["stock"] - $cartinfo -> quantity;
			$update = array("stock" => $stock, "num_sale" => $numsale);
			$this -> db -> where("product_id", $cartinfo -> product_id);
			$this -> db -> update("inventory", $update);
		}

		// end of updating inventory of product...//

		/// making a voucher ...///
		$this -> db -> where("customer_id", $this -> session -> userdata("customer_id"));
		$this -> db -> order_by("purchase_id", "desc");
		$q = $this -> db -> get("purchase");
		$mylastorder = $q -> row_array();
		$getcartquery = $this -> db -> get_where("cart_table", array("customer_id" => $this -> session -> userdata("customer_id")));
		$cartinfo = $getcartquery -> result();
		foreach ($cartinfo as $cartinfo) {
			$voucher = array("purchase_id" => $mylastorder["purchase_id"], "product_id" => $cartinfo -> product_id, "date" => date("Y-m-d"), "quantity" => $cartinfo -> quantity, "selling_price" => $cartinfo -> unit_price, "avg_bought_price" => $cartinfo -> avg_bought_price, "memo" => $cartinfo -> memo);
			$this -> db -> insert("voucher", $voucher);
		}
		// End making the voucher ...///
		/// Updating customerinfo
		$customerquery = $this -> db -> get_where("customerinfo", array("customer_id" => $this -> session -> userdata("customer_id")));
		$customer = $customerquery -> row_array();
		$totalcredit = $customer["total_credit"] - $sum["total"];
		$totalproductpurchased = $customer["total_product_purchased"] + $sum["quantity"];
		$numofpurchase = $customer["num_of_purchase"] + 1;
		$purchaseamount = $customer["purchase_amount"] + $sum["total"];
		$updatecustomer = array("total_credit" => $totalcredit, "num_of_purchase" => $numofpurchase, "total_product_purchased" => $totalproductpurchased, "purchase_amount" => $purchaseamount);
		$this -> db -> where("customer_id", $this -> session -> userdata("customer_id"));
		$this -> db -> update("customerinfo", $updatecustomer);

		/// End of customer info ..///

		///Deleting customer cart ///

		// End of purchase ...///
	}

	public function deletewholecart() {
		$this -> db -> delete("cart_table", array("customer_id" => $this -> session -> userdata("customer_id")));
	}

	public function getcustomer() {
		$query = $this -> db -> get_where("customerinfo", array("customer_id" => $this -> session -> userdata("customer_id")));
		return $query -> row_array();
	}

	public function myorder() {
		$this -> db -> order_by("purchase_id", "desc");
		$query = $this -> db -> get_where("purchase", array("customer_id" => $this -> session -> userdata("customer_id")));
		return $query -> result();
	}

	public function updateprofile() {
		$updateinfo = array("name" => $this -> input -> post("name"), "country" => $this -> input -> post("country"), "state" => $this -> input -> post("state"), "address" => $this -> input -> post("address"), "contact_no" => $this -> input -> post("contact_no"), "email" => $this -> input -> post("email"), "password" => $this -> input -> post("password"), "account_num" => $this -> input -> post("account_num"));
		$this -> db -> where("customer_id", $this -> session -> userdata("customer_id"));
		$this -> db -> update("customerinfo", $updateinfo);
	}

	/////...END ...//

	//Admin database ...///
	public function getlast5order() {
		$query = $this -> db -> query("SELECT  * FROM purchase ORDER BY(`purchase_id`) DESC  LIMIT 5");
		return $query -> result();

	}

	public function overview() {
		$sale = $this -> db -> query("SELECT SUM(`sales`) as total from purchase");
		$data['sale'] = $sale -> result();
		$customer = $this -> db -> query("SELECT COUNT(`customer_id`) as customer from customerinfo WHERE `admin`=0");
		$data['customer'] = $customer -> result();
		$profit = $this -> db -> query("SELECT SUM(`profit`) as profit from purchase");
		$data['profit'] = $profit -> result();
		$order = $this -> db -> query("SELECT count(`purchase_id`) as order_no from purchase");
		$data['order'] = $order -> result();
		$product = $this -> db -> query("SELECT count(`product_id`) as product_no from productinfo");
		$data['product'] = $product -> result();
		return $data;

	}

	public function stock() {

		$stock = $this -> db -> query("SELECT * FROM `inventory` ORDER BY (`stock`-`threshold`) ASC");

		return $stock -> result();

	}

	public function salesreport($data = "") {
		if ($data["dstart"] == "" || $data["dend"] == "") {

			if ($data["dstart"] == "" && $data["dend"] != "") {

			}
			if ($data["dstart"] != "" || $data["dend"] == "") {

			}
			if ($data["dstart"] == "" && $data["dend"] == "") {
				$sales = $this -> db -> query("SELECT SUM(`sales`) as total from purchase ");
				$data['sale'] = $sales -> row_array();
				$profit = $this -> db -> query("SELECT SUM(`profit`) as profit from purchase");
				$data['profit'] = $profit -> row_array();
				$order = $this -> db -> query("SELECT count(`purchase_id`) as order_no from purchase");
				$data['order'] = $order -> row_array();
				$remaining = $this -> db -> query("SELECT SUM(`selling_price` * `stock`) as rem FROM inventory ");
				$data['remaining'] = $remaining -> row_array();
				$product = $this -> db -> query("SELECT count(`product_id`) as product_no from productinfo");
				$data['product'] = $product -> row_array();
				$avgbp = $this -> db -> query("SELECT SUM(`sum_avg_b_price`) as avg FROM purchase ");
				$data['avgbp'] = $avgbp -> row_array();
				$bestsale = $this -> db -> query("SELECT `product_id`,`product_name` FROM inventory ORDER BY num_sale DESC LIMIT 1 ");
				$data['bestsale'] = $bestsale -> row_array();
				$bestcustomer = $this -> db -> query("SELECT `customer_id` ,  name  FROM customerinfo ORDER BY `num_of_purchase` DESC LIMIT 1 ");
				$data['bestcustomer'] = $bestcustomer -> row_array();
				$higheststock = $this -> db -> query("SELECT `product_id` , `product_name` ,`stock` FROM inventory  ORDER BY (`stock` - `threshold` ) DESC  LIMIT 1  ");
				$data['higheststock'] = $higheststock -> row_array();
				$loweststock = $this -> db -> query("SELECT `product_id` , `product_name` ,`stock` FROM inventory  ORDER BY (`stock` - `threshold` ) ASC  LIMIT 1 ");
				$data['loweststock'] = $loweststock -> row_array();
				return $data;
			}
		} else {

		}

	}

	public function salesreportcustomer($per) {
		$this -> db -> order_by("purchase_amount", "desc");
		$query = $this -> db -> get("customerinfo", $per, $this -> uri -> segment(3));
		//$query  = $this -> db -> query("SELECT * FROM customerinfo ORDER BY `purchase_amount` DESC ");
		return $query -> result();
	}

	public function salesreportproduct($per) {
		$this -> db -> select(" productinfo.`product_id`, productinfo.`product_name`,`cat_id`, `date_added`,`promotion`, `promo_from`, `promo_to`, `promo_active` , inventory.avg_bought_price, inventory.`selling_price`, inventory.`stock`,inventory.`threshold`,inventory.`num_sale`");

		$this -> db -> join("inventory", " productinfo.product_id= inventory.product_id ", ' inner ');
		$this -> db -> order_by("inventory.`num_sale`", 'desc');

		$query = $this -> db -> get("productinfo", $per, $this -> uri -> segment(3));
		return $query -> result();
	}

	public function rightnow() {
		$query1 = $this -> db -> query("SELECT COUNT(`purchase_id`) as neworder FROM `purchase` WHERE `date` BETWEEN CURDATE() - 3 AND CURDATE()");
		$data["neworder"] = $query1 -> row_array();
		$query2 = $this -> db -> query("SELECT COUNT(`customer_id`) as newcustomer FROM `customerinfo` WHERE `date` BETWEEN CURDATE() - 8 AND CURDATE()");
		$data["newcustomer"] = $query2 -> row_array();
		$query3 = $this -> db -> query("SELECT * FROM `purchase` WHERE `date` = CURDATE()");
		$data["todaysale"] = $query3 -> row_array();
		$query4 = $this -> db -> query("SELECT COUNT(`product_id`) as lowstock FROM `inventory` WHERE `stock` BETWEEN 0 AND `threshold` + 5");
		$data["lowstock"] = $query4 -> row_array();

		return $data;

	}

	public function newcustomer() {
		$query = $this -> db -> query("SELECT * FROM `customerinfo` ORDER BY `customer_id` DESC LIMIT 5 ");
		return $query -> result();
	}

	public function bestsell() {
		$query = $this -> db -> query("SELECT `product_id`,`product_name`,`avg_bought_price`,`stock`,`selling_price`,`num_sale` ,( `selling_price` * `num_sale` ) as sale FROM `inventory` ORDER BY `num_sale` DESC LIMIT 5");
		return $query -> result();
	}

	public function totalsale($id = "") {
		$query = $this -> db -> query("SELECT (`num_sale` * `selling_price`) as totalsale FROM inventory WHERE `product_id`= " . $id);
		return $query -> row_array();
	}

	public function buyhistory($id = "") {
		$this -> db -> order_by("date", "desc");
		$query = $this -> db -> get_where("buy_records", array("product_id" => $id));
		return $query -> result();
	}

	public function getproductid() {
		$query = $this -> db -> query("SELECT * FROM `productinfo` ");
		return $query -> result();
	}

	public function editinventory() {
		$quantity = $this -> input -> post("quantity");
		$product_id = $this -> input -> post("product_id");
		$bought_price = $this -> input -> post("bought_price");
		$getinvntory = $this -> db -> get_where("inventory", array("product_id" => $product_id));
		$productinfo = $getinvntory -> row_array();
		$totalquantity = $quantity + $productinfo["stock"];
		$avgbprice = ($productinfo["avg_bought_price"] * $productinfo["stock"] + $bought_price * $quantity) / $totalquantity;

		$updateinfo = array("stock" => $totalquantity, "avg_bought_price" => $avgbprice);

		$this -> db -> where("product_id", $product_id);
		$this -> db -> update("inventory", $updateinfo);

		$query = $this -> db -> get_where("inventory", array("product_id" => $product_id));
		return $query -> row_array();

	}

	public function getcatagory() {
		$query = $this -> db -> get_where("catagory", array("parent_cat !=" => 0));
		return $query -> result();
	}

	public function insertproduct($filenamepath = "") {
		$productinfo = array("product_name" => $this -> input -> post("name"), "description" => $this -> input -> post("description"), "image" => $filenamepath, "cat_id" => $this -> input -> post("catagory"), "date_added" => date("Y-m-d"), "product_type" => $this -> input -> post("product_type"), "promotion" => $this -> input -> post("promotion"), "promo_from" => $this -> input -> post("dstart"), "promo_to" => $this -> input -> post("dend"), "promo_active" => $this -> input -> post("promotion_active"));

		$this -> db -> insert("productinfo", $productinfo);
		$query = $this -> db -> query("SELECT MAX(`product_id`) as product_id FROM `productinfo` ");
		$product_id = $query -> row_array();

		$buy_records = array("product_id" => $product_id["product_id"], "bought_quantity" => $this -> input -> post("quantity"), "bought_price" => $this -> input -> post("bought_price"), "date" => date("Y-m-d"));
		$inventory = array("product_id" => $product_id["product_id"], "product_name" => $this -> input -> post("name"), "avg_bought_price" => $this -> input -> post("bought_price"), "stock" => $this -> input -> post("quantity"), "threshold" => $this -> input -> post("threshold"), "selling_price" => $this -> input -> post("selling_price"), "num_sale" => 0);
		$this -> db -> insert("inventory", $inventory);
		$this -> db -> insert("buy_records", $buy_records);

		return $product_id["product_id"];
	}

	public function editproduct($id = "") {
		$updateinfo = array("product_name" => $this -> input -> post("product_name"), "description" => $this -> input -> post("description"), "product_type" => $this -> input -> post("product_type"), "promotion" => $this -> input -> post("promotion"), "promo_from" => $this -> input -> post("dstart"), "promo_to" => $this -> input -> post("dend"), "cat_id" => $this -> input -> post("catagory"));
		$this -> db -> where("product_id", $id);
		$this -> db -> update("productinfo", $updateinfo);
		$updateinventory = array("selling_price" => $this -> input -> post("selling_price"));
		$this -> db -> where("product_id", $id);
		$this -> db -> update("inventory", $updateinventory);
	}

	public function getyear() {
		$query = $this -> db -> query("SELECT DISTINCT EXTRACT(YEAR FROM `date`) as year FROM purchase ");
		return $query -> result();
	}

	public function getyearlyreport($year = "") {
		$query = $this -> db -> query("SELECT EXTRACT(month FROM `date`) as month,COUNT(`purchase_id`) as Order_monthly,SUM(`sales`) as sales,SUM(`total_quantity`)as quantity, SUM( `profit`) as profit  FROM `purchase` WHERE EXTRACT(year FROM `date`) =" . $year . " GROUP BY EXTRACT(month FROM `date`) ");
		return $query -> result();
	}

	public function getacatagory($cat_id = "") {
		$query = $this -> db -> get_where("catagory", array("cat_id" => $cat_id));
		return $query -> row_array();
	}

	public function getparentcatagory() {
		$query = $this -> db -> get_where("catagory", array("parent_cat =" => 0));
		return $query -> result();
	}

	public function updatecatagory() {
		$updateinfo = array("cat_name" => $this -> input -> post("cat_name"), "parent_cat" => $this -> input -> post("catagory"));
		$this -> db -> where("cat_id", $this -> input -> post("cat_id"));
		$this -> db -> update("catagory", $updateinfo);
	}

	public function insertcatagory($filenamepath = "") {
		$insertinfo = array("cat_name" => $this -> input -> post("name"), "image" => $filenamepath, "parent_cat" => $this -> input -> post("catagory"));

		$this -> db -> insert("catagory", $insertinfo);
	}

	public function updatepromotion($id = "") {
		$updateinfo = array("promotion" => $this -> input -> post("promotion"), "promo_from" => $this -> input -> post("dstart"), "promo_to" => $this -> input -> post("dend"), "promo_active" => $this -> input -> post("promo_active"));
		$this -> db -> where("product_id", $id);
		$this -> db -> update("productinfo", $updateinfo);
	}

	public function customerdetails($id = "") {
		$query = $this -> db -> get_where("customerinfo", array("customer_id" => $id));
		return $query -> row_array();
	}

	public function sumprofitcustomer($id = "") {
		$query = $this -> db -> query("SELECT SUM(`profit`) as profit FROM `purchase` WHERE `customer_id`=" . $id);
		return $query -> row_array();
	}

	public function orderofcustomer($id = "") {
		$query = $this -> db -> query("SELECT `purchase_id` FROM `purchase` WHERE `customer_id`=" . $id);
		return $query -> result();
	}

	public function countorder() {
		$query = $this -> db -> query("SELECT COUNT(`purchase_id`) as orderno FROM purchase");
		return $query -> row_array();
	}

	public function allorder() {
		$query = $this -> db -> get("purchase");
		return $query -> result();
	}

	public function deleteorder($id = "") {
		$query = $this -> db -> get_where("voucher", array("purchase_id" => $id));
		$voucher = $query -> result();
		foreach ($voucher as $voucher) {
			$query = $this -> db -> get_where("inventory", array("product_id" => $voucher->product_id));
			$product = $query -> row_array();
			$stock = $product["stock"]+$voucher->quantity;
			$numsale = $product["num_sale"]-$voucher->quantity;
			$updateinfo =array("stock"=>$stock,"num_sale"=>$numsale);
			$this->db->where("product_id",$voucher->product_id);
			$this->db->update("inventory",$updateinfo);
		}
		
		/// updating customer ...///
		$query = $this->db->get_where("purchase",array("purchase_id"=>$id));
		$order = $query->row_array();
		$customerquery=$this->db->get_where("customerinfo",array("customer_id"=>$order["customer_id"]));
		$customer = $customerquery->row_array();
		$numofpurchase = $customer["num_of_purchase"]-1;
		$totalproductpurchase =$customer["total_product_purchased"] - $order["total_quantity"];
		$amountpurchase = $customer["purchase_amount"]-$order["sales"];
		$totalcredit = $customer["total_credit"]+$order["sales"];
		
		$updatecustomer = array("total_credit"=>$totalcredit,"purchase_amount"=>$amountpurchase,"total_product_purchased"=>$totalproductpurchase,"num_of_purchase"=>$numofpurchase);
		$this->db->where("customer_id",$customer["customer_id"]);
		$this->db->update("customerinfo",$updatecustomer);
		
		$this->db->delete("voucher",array("purchase_id"=>$id));
		$this->db->delete("purchase",array("purchase_id"=>$id));
		
	}
public function getaorder($id=""){
	$query = $this->db->get_where("purchase",array("purchase_id"=>$id));
	return $query->row_array();
}
public function getvoucher($id=""){
	$query = $this -> db -> get_where("voucher", array("purchase_id" => $id));
	return $query -> result();
}
public function updateorder($id=""){
	$updateinfo = array("delivery_div"=>$this->input->post("country"),"delivery_address"=>$this->input->post("address"));
	$this->db->where("purchase_id",$id);
	$this->db->update("purchase",$updateinfo);
}

	// END ...//

}
?>