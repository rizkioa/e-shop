<?php
class Customer extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this -> load -> model('edata');
		$this -> load -> helper('form');
		$this -> load -> library('session');
		$this -> load -> helper('url');
	}

	public function index() {
		if ($this -> session -> userdata('is_loged_in')) {
			$data['base'] = $this -> config -> item('base');
			$data['base_url'] = $this -> config -> item('base_url');
			$data['images'] = $this -> config -> item('img_url');

			$data['cart'] = $this -> load -> view('carticon', $data, true);
			$data['main'] = $this -> load -> view('slider', $data, true);
			$data['body'] = $this -> load -> view('productsample', $data, true);
			$data['sidebar'] = $this -> load -> view('search', $data, true);
			$data['menu'] = $this -> load -> view('catagory', $data, true);
			$data['sidefull'] = $this -> load -> view('moreproduct', $data, true);
			$data['last'] = $this -> load -> view('cols', $data, true);
			$data['foot'] = $this -> load -> view('foot', $data, true);
			$this -> load -> view('index', $data);
		} else {
			redirect('http://localhost/online/index.php/main', 'refresh');
		}

	}

	public function logout() {
		$this -> session -> sess_destroy();
		redirect('http://localhost/online/index.php/main', 'refresh');
	}

	public function deliveryarea() {
		if ($this -> session -> userdata('is_loged_in')) {
			$data['base'] = $this -> config -> item('base');
			$data['base_url'] = $this -> config -> item('base_url');
			$data['images'] = $this -> config -> item('img_url');
			$data['deliery'] = $this -> edata -> delivery();
			$data['cart'] = $this -> load -> view('carticon', $data, true);
			$data['main'] = $this -> load -> view('d_areas', $data, true);
			$data['body'] = "";
			$data['sidebar'] = $this -> load -> view('search', $data, true);
			$data['menu'] = $this -> load -> view('catagory', $data, true);
			$data['sidefull'] = $this -> load -> view('moreproduct', $data, true);
			$data['last'] = $this -> load -> view('cols', $data, true);
			$data['foot'] = $this -> load -> view('foot', $data, true);
			$this -> load -> view('index', $data);
		} else {
			redirect('http://localhost/online/index.php/main', 'refresh');
		}
	}

	public function contact() {
		if ($this -> session -> userdata('is_loged_in')) {
			$data['base'] = $this -> config -> item('base');
			$data['base_url'] = $this -> config -> item('base_url');
			$data['images'] = $this -> config -> item('img_url');

			$data['cart'] = $this -> load -> view('carticon', $data, true);
			$data['main'] = $this -> load -> view('suggest', $data, true);
			$data['body'] = "";
			$data['sidebar'] = $this -> load -> view('search', $data, true);
			$data['menu'] = $this -> load -> view('catagory', $data, true);
			$data['sidefull'] = $this -> load -> view('moreproduct', $data, true);
			$data['last'] = $this -> load -> view('cols', $data, true);
			$data['foot'] = $this -> load -> view('foot', $data, true);
			$this -> load -> view('index', $data);
		} else {
			redirect('http://localhost/online/index.php/main', 'refresh');
		}
	}

	public function myaccount() {
		if ($this -> session -> userdata('is_loged_in')) {
			$data['base'] = $this -> config -> item('base');
			$data['base_url'] = $this -> config -> item('base_url');
			$data['images'] = $this -> config -> item('img_url');

			$data['cart'] = $this -> load -> view('carticon', $data, true);
			$data['main'] = $this -> load -> view('myaccount', $data, true);
			$data['body'] = "";
			$data['sidebar'] = $this -> load -> view('search', $data, true);
			$data['menu'] = $this -> load -> view('catagory', $data, true);
			$data['sidefull'] = $this -> load -> view('moreproduct', $data, true);
			$data['last'] = $this -> load -> view('cols', $data, true);
			$data['foot'] = $this -> load -> view('foot', $data, true);
			$this -> load -> view('index', $data);
		} else {
			redirect('http://localhost/online/index.php/main', 'refresh');
		}

	}

	public function myprofile() {
		if ($this -> session -> userdata('is_loged_in')) {
			$data['base'] = $this -> config -> item('base');
			$data['base_url'] = $this -> config -> item('base_url');
			$data['images'] = $this -> config -> item('img_url');
			$data["customer"] = $this -> edata -> getcustomer();
			$data['cart'] = $this -> load -> view('carticon', $data, true);
			$data['main'] = $this -> load -> view('showmyprofile', $data, true);
			$data['body'] = "";
			$data['sidebar'] = $this -> load -> view('search', $data, true);
			$data['menu'] = $this -> load -> view('catagory', $data, true);
			$data['sidefull'] = $this -> load -> view('moreproduct', $data, true);
			$data['last'] = $this -> load -> view('cols', $data, true);
			$data['foot'] = $this -> load -> view('foot', $data, true);
			$this -> load -> view('index', $data);
		} else {
			redirect('http://localhost/online/index.php/main', 'refresh');
		}

	}

	public function myorderlist() {
		if ($this -> session -> userdata('is_loged_in')) {
			$data['base'] = $this -> config -> item('base');
			$data['base_url'] = $this -> config -> item('base_url');
			$data['images'] = $this -> config -> item('img_url');
			$data["order"] = $this -> edata -> myorder();
			$data['cart'] = $this -> load -> view('carticon', $data, true);
			$data['main'] = $this -> load -> view('myorder', $data, true);
			$data['body'] = "";
			$data['sidebar'] = $this -> load -> view('search', $data, true);
			$data['menu'] = $this -> load -> view('catagory', $data, true);
			$data['sidefull'] = $this -> load -> view('moreproduct', $data, true);
			$data['last'] = $this -> load -> view('cols', $data, true);
			$data['foot'] = $this -> load -> view('foot', $data, true);
			$this -> load -> view('index', $data);
		} else {
			redirect('http://localhost/online/index.php/main', 'refresh');
		}
	}
	
	public function myvoucher($id="") {
		if ($this -> session -> userdata('is_loged_in')) {
			$data['base'] = $this -> config -> item('base');
			$data['base_url'] = $this -> config -> item('base_url');
			$data['images'] = $this -> config -> item('img_url');
			$data["voucher"] = $this -> edata -> getvoucher($id);
			$data["order"] = $this -> edata -> myorder();
			$data['cart'] = $this -> load -> view('carticon', $data, true);
			$data['main'] = $this -> load -> view('myvoucher', $data, true);
			$data['body'] ="";
			$data['sidebar'] = $this -> load -> view('search', $data, true);
			$data['menu'] = $this -> load -> view('catagory', $data, true);
			$data['sidefull'] = $this -> load -> view('moreproduct', $data, true);
			$data['last'] = $this -> load -> view('cols', $data, true);
			$data['foot'] = $this -> load -> view('foot', $data, true);
			$this -> load -> view('index', $data);
		} else {
			redirect('http://localhost/online/index.php/main', 'refresh');
		}
	}
	
	public function deleteorder($id=""){
		if ($this -> session -> userdata('is_loged_in')) {
			$data['base'] = $this -> config -> item('base');
			$data['base_url'] = $this -> config -> item('base_url');
			$data['images'] = $this -> config -> item('img_url');
			$this->edata->deleteorder($id);
			$data["order"] = $this -> edata -> myorder();
			$data['cart'] = $this -> load -> view('carticon', $data, true);
			$data['main'] = $this -> load -> view('myorder', $data, true);
			$data['body'] = "Order Has been Canceled";
			$data['sidebar'] = $this -> load -> view('search', $data, true);
			$data['menu'] = $this -> load -> view('catagory', $data, true);
			$data['sidefull'] = $this -> load -> view('moreproduct', $data, true);
			$data['last'] = $this -> load -> view('cols', $data, true);
			$data['foot'] = $this -> load -> view('foot', $data, true);
			$this -> load -> view('index', $data);
		} else {
			redirect('http://localhost/online/index.php/main', 'refresh');
		}
	}

	public function editmyprofile() {
		if ($this -> session -> userdata('is_loged_in')) {
			$data['base'] = $this -> config -> item('base');
			$data['base_url'] = $this -> config -> item('base_url');
			$data['images'] = $this -> config -> item('img_url');
			$data["customer"] = $this -> edata -> getcustomer();
			$data['cart'] = $this -> load -> view('carticon', $data, true);
			$data['main'] = $this -> load -> view("editmyprofile", $data, true);
			$data['body'] = "";
			$data['sidebar'] = $this -> load -> view('search', $data, true);
			$data['menu'] = $this -> load -> view('catagory', $data, true);
			$data['sidefull'] = $this -> load -> view('moreproduct', $data, true);
			$data['last'] = $this -> load -> view('cols', $data, true);
			$data['foot'] = $this -> load -> view('foot', $data, true);
			$this -> load -> view('index', $data);
		} else {
			redirect('http://localhost/online/index.php/main', 'refresh');
		}
	}

	public function updatemyprofile() {
		if ($this -> session -> userdata('is_loged_in')) {
			$data['base'] = $this -> config -> item('base');
			$data['base_url'] = $this -> config -> item('base_url');
			$data['images'] = $this -> config -> item('img_url');
			/// Update profile..//
			$success = $this -> edata -> updateprofile();
			$data["customer"] = $this -> edata -> getcustomer();
			$data['cart'] = $this -> load -> view('carticon', $data, true);
			$data['main'] = $this -> load -> view("showmyprofile", $data, true);
			$data['body'] = "Your Profile has succesfully Updated";
			$data['sidebar'] = $this -> load -> view('search', $data, true);
			$data['menu'] = $this -> load -> view('catagory', $data, true);
			$data['sidefull'] = $this -> load -> view('moreproduct', $data, true);
			$data['last'] = $this -> load -> view('cols', $data, true);
			$data['foot'] = $this -> load -> view('foot', $data, true);
			$this -> load -> view('index', $data);
		} else {
			redirect('http://localhost/online/index.php/main', 'refresh');
		}
	}

	public function showcart() {
		if ($this -> session -> userdata('is_loged_in')) {
			$data['base'] = $this -> config -> item('base');
			$data['base_url'] = $this -> config -> item('base_url');
			$data['images'] = $this -> config -> item('img_url');
			$data["cartinfo"] = $this -> edata -> getcart();
			$data['price'] = $this -> edata -> sumcart();
			$data['cart'] = $this -> load -> view('carticon', $data, true);
			$data['main'] = $this -> load -> view('shoppingcart', $data, true);
			$data['body'] = "";
			$data['sidebar'] = $this -> load -> view('search', $data, true);
			$data['menu'] = $this -> load -> view('catagory', $data, true);
			$data['sidefull'] = $this -> load -> view('moreproduct', $data, true);
			$data['last'] = $this -> load -> view('cols', $data, true);
			$data['foot'] = $this -> load -> view('foot', $data, true);
			$this -> load -> view('index', $data);
		} else {
			redirect('http://localhost/online/index.php/main', 'refresh');
		}
	}

	public function addcart($id = "") {

		if ($this -> session -> userdata('is_loged_in')) {
			$data['base'] = $this -> config -> item('base');
			$data['base_url'] = $this -> config -> item('base_url');
			$data['images'] = $this -> config -> item('img_url');
			$quantity = $this -> input -> post('quantity');
			if ($quantity == "" || $quantity == 0) {

				$data['cart'] = $this -> load -> view('carticon', $data, true);
				$data['main'] = "You Must Fill up the quantity";
				$data['body'] = "";
				$data['sidebar'] = $this -> load -> view('search', $data, true);
				$data['menu'] = $this -> load -> view('catagory', $data, true);
				$data['sidefull'] = $this -> load -> view('moreproduct', $data, true);
				$data['last'] = $this -> load -> view('cols', $data, true);
				$data['foot'] = $this -> load -> view('foot', $data, true);
				$this -> load -> view('index', $data);
			} else {
				$productinfo = $this -> edata -> productdetails($id);
				$this -> edata -> insertcart($productinfo);
				$data["cartinfo"] = $this -> edata -> getcart();
				$data['price'] = $this -> edata -> sumcart();
				//..///
				$data['cart'] = $this -> load -> view('carticon', $data, true);
				$data['main'] = $this -> load -> view('shoppingcart', $data, true);
				$data['body'] = "";
				$data['sidebar'] = $this -> load -> view('search', $data, true);
				$data['menu'] = $this -> load -> view('catagory', $data, true);
				$data['sidefull'] = $this -> load -> view('moreproduct', $data, true);
				$data['last'] = $this -> load -> view('cols', $data, true);
				$data['foot'] = $this -> load -> view('foot', $data, true);
				$this -> load -> view('index', $data);
			}
			/// Database ...///

		} else {
			redirect('http://localhost/online/index.php/main', 'refresh');
		}
	}

	public function deletecart($id = "") {

		if ($this -> session -> userdata('is_loged_in')) {
			$data['base'] = $this -> config -> item('base');
			$data['base_url'] = $this -> config -> item('base_url');
			$data['images'] = $this -> config -> item('img_url');
			/// Database ...///

			$this -> edata -> deletecart($id);
			$data["cartinfo"] = $this -> edata -> getcart();
			$data['price'] = $this -> edata -> sumcart();
			//..///
			$data['cart'] = $this -> load -> view('carticon', $data, true);
			$data['main'] = $this -> load -> view('shoppingcart', $data, true);
			$data['body'] = "";
			$data['sidebar'] = $this -> load -> view('search', $data, true);
			$data['menu'] = $this -> load -> view('catagory', $data, true);
			$data['sidefull'] = $this -> load -> view('moreproduct', $data, true);
			$data['last'] = $this -> load -> view('cols', $data, true);
			$data['foot'] = $this -> load -> view('foot', $data, true);
			$this -> load -> view('index', $data);
		} else {
			redirect('http://localhost/online/index.php/main', 'refresh');
		}
	}

	public function showeditcartform($id = "") {
		echo $id;
		echo $this -> input -> post("quantity");
	}

	public function editcart($id = "") {

		if ($this -> session -> userdata('is_loged_in')) {
			$data['base'] = $this -> config -> item('base');
			$data['base_url'] = $this -> config -> item('base_url');
			$data['images'] = $this -> config -> item('img_url');
			/// Database ...///
			if ($this -> input -> post("quantity") != 0 &&  $this -> input -> post("quantity") != "") {
				$productinfo = $this -> edata -> productdetails($id);
				$this -> edata -> editcart($productinfo);
			}

			$data["cartinfo"] = $this -> edata -> getcart();
			$data['price'] = $this -> edata -> sumcart();
			//..///
			$data['cart'] = $this -> load -> view('carticon', $data, true);
			$data['main'] = $this -> load -> view('shoppingcart', $data, true);
			if ($this -> input -> post("quantity") == 0 || $this -> input -> post("quantity") == "") {
				$data['body'] = "Wrong Quantity Inserted";
			} else {
				$data['body'] = "Quantity Updated succefully";
			}
			$data['sidebar'] = $this -> load -> view('search', $data, true);
			$data['menu'] = $this -> load -> view('catagory', $data, true);
			$data['sidefull'] = $this -> load -> view('moreproduct', $data, true);
			$data['last'] = $this -> load -> view('cols', $data, true);
			$data['foot'] = $this -> load -> view('foot', $data, true);
			$this -> load -> view('index', $data);
		} else {
			redirect('http://localhost/online/index.php/main', 'refresh');
		}
	}

	public function purchaseform() {
		if ($this -> session -> userdata('is_loged_in')) {
			$data['base'] = $this -> config -> item('base');
			$data['base_url'] = $this -> config -> item('base_url');
			$data['images'] = $this -> config -> item('img_url');
			$data["cartinfo"] = $this -> edata -> getcart();
			$data['price'] = $this -> edata -> sumcart();
			$data['cart'] = $this -> load -> view('carticon', $data, true);
			$data['main'] = $this -> load -> view('purchaseform', $data, true);
			$data['body'] = "";
			$data['sidebar'] = $this -> load -> view('search', $data, true);
			$data['menu'] = $this -> load -> view('catagory', $data, true);
			$data['sidefull'] = $this -> load -> view('moreproduct', $data, true);
			$data['last'] = $this -> load -> view('cols', $data, true);
			$data['foot'] = $this -> load -> view('foot', $data, true);
			$this -> load -> view('index', $data);
		} else {
			redirect('http://localhost/online/index.php/main', 'refresh');
		}
	}

	public function dopurchase() {

		if ($this -> session -> userdata('is_loged_in')) {
			$success = $this -> edata -> findsum();
			// Detecting if the quantity of the product is less than the stock ...//
			if ($success["success"] == true) {/// true mane product quantity select thik ase ...///
				/// Ashol kaj eikhane hobe ...///
				$verifyaccount = $this -> edata -> verifyaccount();
				if ($verifyaccount["success"] == true) {
					/// Able to purchase
					$result = $this -> edata -> dopurchase($verifyaccount["total_credit"]);

					$data['base'] = $this -> config -> item('base');
					$data['base_url'] = $this -> config -> item('base_url');
					$data['images'] = $this -> config -> item('img_url');
					$data["cartinfo"] = $this -> edata -> getcart();
					$data['price'] = $this -> edata -> sumcart();
					$data['cart'] = $this -> load -> view('carticon', $data, true);
					$data['main'] = $this -> load -> view('purchasereport', $data, true);
					$data['body'] = "";
					$data['sidebar'] = $this -> load -> view('search', $data, true);
					$data['menu'] = $this -> load -> view('catagory', $data, true);
					$data['sidefull'] = $this -> load -> view('moreproduct', $data, true);
					$data['last'] = $this -> load -> view('cols', $data, true);
					$data['foot'] = $this -> load -> view('foot', $data, true);
					$this -> load -> view('index', $data);
					$this -> edata -> deletewholecart();
				} else {
					/// Unable to purchase .../// Due to Failed in varifying account
					$data['base'] = $this -> config -> item('base');
					$data['base_url'] = $this -> config -> item('base_url');
					$data['images'] = $this -> config -> item('img_url');
					$data["cartinfo"] = $this -> edata -> getcart();
					$data['price'] = $this -> edata -> sumcart();
					$data['cart'] = $this -> load -> view('carticon', $data, true);
					$data['main'] = $this -> load -> view('purchaseform', $data, true);
					$data['body'] = "<font color='red'>::: Warning :::" . $verifyaccount["message"] . "</font> ";
					$data['sidebar'] = $this -> load -> view('search', $data, true);
					$data['menu'] = $this -> load -> view('catagory', $data, true);
					$data['sidefull'] = $this -> load -> view('moreproduct', $data, true);
					$data['last'] = $this -> load -> view('cols', $data, true);
					$data['foot'] = $this -> load -> view('foot', $data, true);
					$this -> load -> view('index', $data);
				}
			} else {
				$data['base'] = $this -> config -> item('base');
				$data['base_url'] = $this -> config -> item('base_url');
				$data['images'] = $this -> config -> item('img_url');
				$data["cartinfo"] = $this -> edata -> getcart();
				$data['price'] = $this -> edata -> sumcart();
				$data['cart'] = $this -> load -> view('carticon', $data, true);
				$data['main'] = $this -> load -> view('shoppingcart', $data, true);
				if ($success["product"] != 0) {
					$data['body'] = " <p> <font color='red'> Warning ::: We are extreamly sorry to inform you that the number product with the id no : " . $success["product"] . " that you have ordered is not available at our stock currently. Please edit your quantity </font></p>";
				} else {
					$data['body'] = " <p><font color='red'>Warning ::: You have not Choose any product!!!</font></p>";
				}

				$data['sidebar'] = $this -> load -> view('search', $data, true);
				$data['menu'] = $this -> load -> view('catagory', $data, true);
				$data['sidefull'] = $this -> load -> view('moreproduct', $data, true);
				$data['last'] = $this -> load -> view('cols', $data, true);
				$data['foot'] = $this -> load -> view('foot', $data, true);
				$this -> load -> view('index', $data);
			}

		} else {
			redirect('http://localhost/online/index.php/main', 'refresh');
		}

	}

}
?>