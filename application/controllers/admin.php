<?php
class Admin extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this -> load -> model('edata');
		$this -> load -> helper('form');
		$this -> load -> library('session');
		$this -> load -> helper('url');
		$this -> load -> library('pagination');
		$this -> load -> library('upload');
	}

	public function index() {
		if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") == 1) {

			$data['base'] = $this -> config -> item('base');
			$data['panel'] = $this -> load -> view('dashbordpanel', $data, true);
			//msg showing the total overviw of the store ..database queries must be needed but not yet implemented..//
			$data['msg'] = $this -> load -> view('rightnow', $data, true);
			$data['content'] = $this -> load -> view('admininfobox', $data, true);
			//......//

			$this -> load -> view('adminindex', $data);
		} else {
			if ($this -> session -> userdata("is_loged_in") == false) {
				redirect('http://localhost/online/index.php/main/login', 'refresh');
			}
			if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") != 1) {
				redirect('http://localhost/online/index.php/customer', 'refresh');
			}

		}

	}

	public function orders() {

		if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") == 1) {
			$data['base'] = $this -> config -> item('base');
			$data['panel'] = $this -> load -> view('orderpanel', $data, true);
			//msg showing the total overviw of the store ..database queries must be needed but not yet implemented..//
			$data['msg'] = $this -> load -> view('ordermsg', $data, true);
			$data['content'] = $this -> load -> view('orderinfobox', $data, true);
			//......//

			$this -> load -> view('adminindex', $data);

		} else {
			if ($this -> session -> userdata("is_loged_in") == false) {
				redirect('http://localhost/online/index.php/main/login', 'refresh');
			}
			if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") != 1) {
				redirect('http://localhost/online/index.php/customer', 'refresh');
			}

		}

	}

	public function showallorder() {

		if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") == 1) {
			$data['base'] = $this -> config -> item('base');
			$data['panel'] = $this -> load -> view('orderpanel', $data, true);
			//msg showing the total overviw of the store ..database queries must be needed but not yet implemented..//
			$data['msg'] = "<h1>All Orders</h1>";
			$data['content'] = $this -> load -> view('allorder', $data, true);
			//......//

			$this -> load -> view('adminindex', $data);

		} else {
			if ($this -> session -> userdata("is_loged_in") == false) {
				redirect('http://localhost/online/index.php/main/login', 'refresh');
			}
			if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") != 1) {
				redirect('http://localhost/online/index.php/customer', 'refresh');
			}

		}
	}

	public function showneworder() {

		if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") == 1) {
			$data['base'] = $this -> config -> item('base');
			$data['panel'] = $this -> load -> view('orderpanel', $data, true);
			//msg showing the total overviw of the store ..database queries must be needed but not yet implemented..//
			$data['msg'] = "<h1>Latest Orders</h1>";
			$data['content'] = $this -> load -> view('neworder', $data, true);
			//......//

			$this -> load -> view('adminindex', $data);

		} else {
			if ($this -> session -> userdata("is_loged_in") == false) {
				redirect('http://localhost/online/index.php/main/login', 'refresh');
			}
			if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") != 1) {
				redirect('http://localhost/online/index.php/customer', 'refresh');
			}

		}
	}

	public function deleteorder($id = "") {
		if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") == 1) {
			$data['base'] = $this -> config -> item('base');
			$data['panel'] = $this -> load -> view('orderpanel', $data, true);
			//msg showing the total overviw of the store ..database queries must be needed but not yet implemented..//
			$this -> edata -> deleteorder($id);
			///
			$data['msg'] = "<h1>All Orders</h1>";
			$data['content'] = $this -> load -> view('allorder', $data, true);
			//......//

			$this -> load -> view('adminindex', $data);

		} else {
			if ($this -> session -> userdata("is_loged_in") == false) {
				redirect('http://localhost/online/index.php/main/login', 'refresh');
			}
			if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") != 1) {
				redirect('http://localhost/online/index.php/customer', 'refresh');
			}

		}
	}

	public function detailsorder($id = "") {
		if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") == 1) {
			$data['base'] = $this -> config -> item('base');
			$data['panel'] = $this -> load -> view('orderpanel', $data, true);
			//msg showing the total overviw of the store ..database queries must be needed but not yet implemented..//
			$data["order"] = $this -> edata -> getaorder($id);
			$data["voucher"] = $this -> edata -> getvoucher($id);
			///
			$data['msg'] = "<h1>Order Details</h1>";
			$data['content'] = $this -> load -> view('orderdetails', $data, true);
			//......//

			$this -> load -> view('adminindex', $data);

		} else {
			if ($this -> session -> userdata("is_loged_in") == false) {
				redirect('http://localhost/online/index.php/main/login', 'refresh');
			}
			if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") != 1) {
				redirect('http://localhost/online/index.php/customer', 'refresh');
			}

		}
	}

	public function showeditorder($id = "") {
		if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") == 1) {
			$data['base'] = $this -> config -> item('base');
			$data['panel'] = $this -> load -> view('orderpanel', $data, true);
			//msg showing the total overviw of the store ..database queries must be needed but not yet implemented..//
			$data["order"] = $this -> edata -> getaorder($id);
			///
			$data['msg'] = "<h1>Edit Order</h1>";
			$data['content'] = $this -> load -> view('editorderform', $data, true);
			//......//

			$this -> load -> view('adminindex', $data);

		} else {
			if ($this -> session -> userdata("is_loged_in") == false) {
				redirect('http://localhost/online/index.php/main/login', 'refresh');
			}
			if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") != 1) {
				redirect('http://localhost/online/index.php/customer', 'refresh');
			}

		}
	}

	public function updateorder($id = "") {
		if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") == 1) {
			$data['base'] = $this -> config -> item('base');
			$data['panel'] = $this -> load -> view('orderpanel', $data, true);

			//msg showing the total overviw of the store ..database queries must be needed but not yet implemented..//
			$this -> edata -> updateorder($id);
			$data["order"] = $this -> edata -> getaorder($id);
			$data["voucher"] = $this -> edata -> getvoucher($id);
			///
			$data['msg'] = "<h1>Order Details</h1>";
			$data['content'] = $this -> load -> view('orderdetails', $data, true);
			//......//

			$this -> load -> view('adminindex', $data);

		} else {
			if ($this -> session -> userdata("is_loged_in") == false) {
				redirect('http://localhost/online/index.php/main/login', 'refresh');
			}
			if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") != 1) {
				redirect('http://localhost/online/index.php/customer', 'refresh');
			}

		}
	}

	public function users() {

		if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") == 1) {
			$data['base'] = $this -> config -> item('base');
			$data['panel'] = $this -> load -> view('userpanel', $data, true);
			//msg showing the total overviw of the store ..database queries must be needed but not yet implemented..//
			$data['msg'] = $this -> load -> view('usrmsg', $data, true);
			$data['content'] = $this -> load -> view('userinfobox', $data, true);
			//......//

			$this -> load -> view('adminindex', $data);
		} else {
			if ($this -> session -> userdata("is_loged_in") == false) {
				redirect('http://localhost/online/index.php/main/login', 'refresh');
			}
			if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") != 1) {
				redirect('http://localhost/online/index.php/customer', 'refresh');
			}

		}

	}

	public function manage() {

		if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") == 1) {
			$data['base'] = $this -> config -> item('base');
			$data['panel'] = $this -> load -> view('managepanel', $data, true);
			//msg showing the total overviw of the store ..database queries must be needed but not yet implemented..//
			$data['msg'] = $this -> load -> view('managemsg', $data, true);
			$data['content'] = $this -> load -> view('manageinfobox', $data, true);
			//......//

			$this -> load -> view('adminindex', $data);
		} else {
			if ($this -> session -> userdata("is_loged_in") == false) {
				redirect('http://localhost/online/index.php/main/login', 'refresh');
			}
			if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") != 1) {
				redirect('http://localhost/online/index.php/customer', 'refresh');
			}

		}

	}

	public function promotion() {
		if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") == 1) {
			$data['base'] = $this -> config -> item('base');
			$data['panel'] = $this -> load -> view('managepanel', $data, true);
			//msg showing the total overviw of the store ..database queries must be needed but not yet implemented..//
			$data['msg'] = $this -> load -> view('selectproduct', $data, true);
			if ($this -> input -> post("product_id") == "") {
				$data['content'] = "please select product id";
			} else {
				$data["product"] = $this -> edata -> productdetails($this -> input -> post("product_id"));
				$data['content'] = $this -> load -> view('promotionform', $data, true);
			}

			//......//

			$this -> load -> view('adminindex', $data);
		} else {
			if ($this -> session -> userdata("is_loged_in") == false) {
				redirect('http://localhost/online/index.php/main/login', 'refresh');
			}
			if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") != 1) {
				redirect('http://localhost/online/index.php/customer', 'refresh');
			}

		}
	}

	public function confirmpromotion($id = "") {
		if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") == 1) {
			//
			$this -> edata -> updatepromotion($id);
			//......//
			redirect('http://localhost/online/index.php/admin/detailsofproduct/' . $id, 'refresh');

		} else {
			if ($this -> session -> userdata("is_loged_in") == false) {
				redirect('http://localhost/online/index.php/main/login', 'refresh');
			}
			if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") != 1) {
				redirect('http://localhost/online/index.php/customer', 'refresh');
			}

		}
	}

	public function catagory() {

		if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") == 1) {
			$data['base'] = $this -> config -> item('base');
			$data['panel'] = $this -> load -> view('managepanel', $data, true);
			//msg showing the total overviw of the store ..database queries must be needed but not yet implemented..//
			$data['msg'] = $this -> load -> view('catagoryaddlist', $data, true);
			$data['content'] = $this -> load -> view('showcatagory', $data, true);
			//......//

			$this -> load -> view('adminindex', $data);
		} else {
			if ($this -> session -> userdata("is_loged_in") == false) {
				redirect('http://localhost/online/index.php/main/login', 'refresh');
			}
			if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") != 1) {
				redirect('http://localhost/online/index.php/customer', 'refresh');
			}

		}
	}

	public function editcatagory() {
		if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") == 1) {
			$data['base'] = $this -> config -> item('base');
			$data['panel'] = $this -> load -> view('managepanel', $data, true);
			//msg showing the total overviw of the store ..database queries must be needed but not yet implemented..//
			$data['msg'] = $this -> load -> view('editcurrentcatagory', $data, true);
			$data['content'] = "";
			//......//

			$this -> load -> view('adminindex', $data);
		} else {
			if ($this -> session -> userdata("is_loged_in") == false) {
				redirect('http://localhost/online/index.php/main/login', 'refresh');
			}
			if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") != 1) {
				redirect('http://localhost/online/index.php/customer', 'refresh');
			}

		}
	}

	public function addnewcatagory() {
		if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") == 1) {
			$data['base'] = $this -> config -> item('base');
			$data['panel'] = $this -> load -> view('managepanel', $data, true);
			//msg showing the total overviw of the store ..database queries must be needed but not yet implemented..//
			$data['msg'] = "Please Fill the following field";
			$data['content'] = $this -> load -> view('addnewcatagory', $data, true);
			//......//

			$this -> load -> view('adminindex', $data);
		} else {
			if ($this -> session -> userdata("is_loged_in") == false) {
				redirect('http://localhost/online/index.php/main/login', 'refresh');
			}
			if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") != 1) {
				redirect('http://localhost/online/index.php/customer', 'refresh');
			}

		}
	}

	public function insertcatagory() {
		if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") == 1) {
			$config['upload_path'] = './img/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size'] = '2048';
			$config['max_width'] = '1024';
			$config['max_height'] = '800';
			$this -> upload -> initialize($config);
			$this -> load -> library('upload', $config);

			if (!$this -> upload -> do_upload('file')) {
				echo "something went wrong, display errors" . $this -> upload -> display_errors();
			} else {
				$upload_data = $this -> upload -> data();
				$filenamepath = "http://localhost/online/img/" . $upload_data['file_name'];
				$data['base'] = $this -> config -> item('base');
				$data['panel'] = $this -> load -> view('managepanel', $data, true);
				//Database //
				$this -> edata -> insertcatagory($filenamepath);
				//Database//
				$data['msg'] = "Catagory Inserted Succefully...";
				$data['content'] = $this -> load -> view('showcatagory', $data, true);
				//......//

				$this -> load -> view('adminindex', $data);

			}

		} else {
			if ($this -> session -> userdata("is_loged_in") == false) {
				redirect('http://localhost/online/index.php/main/login', 'refresh');
			}
			if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") != 1) {
				redirect('http://localhost/online/index.php/customer', 'refresh');
			}

		}
	}

	public function updatecatagory() {
		if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") == 1) {
			$data['base'] = $this -> config -> item('base');
			$data['panel'] = $this -> load -> view('managepanel', $data, true);
			//msg showing the total overviw of the store ..database queries must be needed but not yet implemented..//
			$this -> edata -> updatecatagory();
			//
			$data['msg'] = "Catagory is Updated Succefully...";
			$data['content'] = $this -> load -> view('showcatagory', $data, true);
			//......//

			$this -> load -> view('adminindex', $data);
		} else {
			if ($this -> session -> userdata("is_loged_in") == false) {
				redirect('http://localhost/online/index.php/main/login', 'refresh');
			}
			if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") != 1) {
				redirect('http://localhost/online/index.php/customer', 'refresh');
			}

		}
	}

	public function buyproducts() {

		if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") == 1) {
			$data['base'] = $this -> config -> item('base');
			$data['panel'] = $this -> load -> view('managepanel', $data, true);
			//msg showing the total overviw of the store ..database queries must be needed but not yet implemented..//
			$data['msg'] = $this -> load -> view('showbuycatagory', $data, true);
			$data['content'] = "";
			//......//

			$this -> load -> view('adminindex', $data);
		} else {
			if ($this -> session -> userdata("is_loged_in") == false) {
				redirect('http://localhost/online/index.php/main/login', 'refresh');
			}
			if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") != 1) {
				redirect('http://localhost/online/index.php/customer', 'refresh');
			}

		}
	}

	public function buycurrentproduct() {
		if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") == 1) {
			$data['base'] = $this -> config -> item('base');
			$data['panel'] = $this -> load -> view('managepanel', $data, true);
			//msg showing the total overviw of the store ..database queries must be needed but not yet implemented..//
			$data['msg'] = $this -> load -> view('curproductpurchaseform', $data, true);
			$data['content'] = "";
			//......//

			$this -> load -> view('adminindex', $data);
		} else {
			if ($this -> session -> userdata("is_loged_in") == false) {
				redirect('http://localhost/online/index.php/main/login', 'refresh');
			}
			if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") != 1) {
				redirect('http://localhost/online/index.php/customer', 'refresh');
			}

		}
	}

	public function addcurrentprodutinventory() {
		if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") == 1) {
			$data['base'] = $this -> config -> item('base');
			$data['panel'] = $this -> load -> view('managepanel', $data, true);
			//msg showing the total overviw of the store ..database queries must be needed but not yet implemented..//

			$data["product"] = $this -> edata -> productdetails($this -> input -> post("product_id"));
			/// Database

			$data['msg'] = "";
			$data['content'] = $this -> load -> view('productdetsalereport', $data, true);
			//......//

			$this -> load -> view('adminindex', $data);
		} else {
			if ($this -> session -> userdata("is_loged_in") == false) {
				redirect('http://localhost/online/index.php/main/login', 'refresh');
			}
			if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") != 1) {
				redirect('http://localhost/online/index.php/customer', 'refresh');
			}

		}
	}

	public function addnewproductform() {
		if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") == 1) {
			$data['base'] = $this -> config -> item('base');
			$data['panel'] = $this -> load -> view('managepanel', $data, true);
			//msg showing the total overviw of the store ..database queries must be needed but not yet implemented..//

			/// Database

			$data['msg'] = "";
			$data['content'] = $this -> load -> view('newproductaddform', $data, true);
			//......//

			$this -> load -> view('adminindex', $data);
		} else {
			if ($this -> session -> userdata("is_loged_in") == false) {
				redirect('http://localhost/online/index.php/main/login', 'refresh');
			}
			if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") != 1) {
				redirect('http://localhost/online/index.php/customer', 'refresh');
			}

		}
	}

	public function addnewproduct() {
		if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") == 1) {
			$config['upload_path'] = './img/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size'] = '2048';
			$config['max_width'] = '1024';
			$config['max_height'] = '800';
			$this -> upload -> initialize($config);
			$this -> load -> library('upload', $config);

			if (!$this -> upload -> do_upload('file')) {
				echo "something went wrong, display errors" . $this -> upload -> display_errors();
			} else {
				$upload_data = $this -> upload -> data();
				$filenamepath = "http://localhost/online/img/" . $upload_data['file_name'];
				$data['base'] = $this -> config -> item('base');
				$data['panel'] = $this -> load -> view('managepanel', $data, true);
				//Database //
				$product_id = $this -> edata -> insertproduct($filenamepath);
				$data["product"] = $this -> edata -> productdetails($product_id);
				//Database//
				$data['msg'] = "";
				$data['content'] = $this -> load -> view('productdetsalereport', $data, true);
				//......//

				$this -> load -> view('adminindex', $data);

			}

		} else {
			if ($this -> session -> userdata("is_loged_in") == false) {
				redirect('http://localhost/online/index.php/main/login', 'refresh');
			}
			if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") != 1) {
				redirect('http://localhost/online/index.php/customer', 'refresh');
			}

		}

	}

	public function products() {
		if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") == 1) {

			$data['base'] = $this -> config -> item('base');
			$data['panel'] = $this -> load -> view('managepanel', $data, true);
			//msg showing the total overviw of the store ..database queries must be needed but not yet implemented..//
			$data['msg'] = "";
			$config['base_url'] = "http://localhost/online/index.php/admin/products/";
			$config['total_rows'] = $this -> db -> get("productinfo") -> num_rows();
			$config['per_page'] = '2';
			$config['full_tag_open'] = '  <div class="pagination">  ';
			$config['full_tag_close'] = '  </div>';

			$this -> pagination -> initialize($config);
			///.....////
			$data["report"] = $this -> edata -> salesreportproduct($config['per_page']);

			$data['content'] = $this -> load -> view('allproduct', $data, true);
			//......//

			$this -> load -> view('adminindex', $data);
		} else {
			if ($this -> session -> userdata("is_loged_in") == false) {
				redirect('http://localhost/online/index.php/main/login', 'refresh');
			}
			if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") != 1) {
				redirect('http://localhost/online/index.php/customer', 'refresh');
			}

		}
	}

	public function detailsofproduct($id = "") {
		if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") == 1) {

			$data['base'] = $this -> config -> item('base');
			$data['panel'] = $this -> load -> view('managepanel', $data, true);
			//Database //
			$data["product"] = $this -> edata -> productdetails($id);
			//Database//
			$data['msg'] = "";
			$data['content'] = $this -> load -> view('detailsofproduct', $data, true);
			//......//

			$this -> load -> view('adminindex', $data);
		} else {
			if ($this -> session -> userdata("is_loged_in") == false) {
				redirect('http://localhost/online/index.php/main/login', 'refresh');
			}
			if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") != 1) {
				redirect('http://localhost/online/index.php/customer', 'refresh');
			}

		}
	}

	public function editproduct($id = "") {
		if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") == 1) {

			$data['base'] = $this -> config -> item('base');
			$data['panel'] = $this -> load -> view('managepanel', $data, true);
			//Database //
			$data["product"] = $this -> edata -> productdetails($id);
			//Database//
			$data['msg'] = "";
			$data['content'] = $this -> load -> view('editproduct', $data, true);
			//......//

			$this -> load -> view('adminindex', $data);
		} else {
			if ($this -> session -> userdata("is_loged_in") == false) {
				redirect('http://localhost/online/index.php/main/login', 'refresh');
			}
			if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") != 1) {
				redirect('http://localhost/online/index.php/customer', 'refresh');
			}

		}
	}

	public function doeditproduct($id = "") {
		if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") == 1) {

			$data['base'] = $this -> config -> item('base');
			$data['panel'] = $this -> load -> view('managepanel', $data, true);
			//Database //
			$this -> edata -> editproduct($id);
			$data["product"] = $this -> edata -> productdetails($id);
			//Database//
			$data['msg'] = "";
			$data['content'] = $this -> load -> view('detailsofproduct', $data, true);
			//......//

			$this -> load -> view('adminindex', $data);
		} else {
			if ($this -> session -> userdata("is_loged_in") == false) {
				redirect('http://localhost/online/index.php/main/login', 'refresh');
			}
			if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") != 1) {
				redirect('http://localhost/online/index.php/customer', 'refresh');
			}

		}
	}

	public function message() {

		if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") == 1) {

			$data['base'] = $this -> config -> item('base');
			$data['panel'] = "";
			//msg showing the total overviw of the store ..database queries must be needed but not yet implemented..//
			$data['msg'] = $this -> load -> view('msgmsg', $data, true);
			$data['content'] = $this -> load -> view('msginfobox', $data, true);
			//......//

			$this -> load -> view('adminindex', $data);
		} else {
			if ($this -> session -> userdata("is_loged_in") == false) {
				redirect('http://localhost/online/index.php/main/login', 'refresh');
			}
			if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") != 1) {
				redirect('http://localhost/online/index.php/customer', 'refresh');
			}

		}

	}

	public function sales() {
		if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") == 1) {

			$data['base'] = $this -> config -> item('base');
			$data['panel'] = $this -> load -> view('dashbordpanel', $data, true);
			//msg showing the total overviw of the store ..database queries must be needed but not yet implemented..//
			$data['msg'] = $this -> load -> view('sales', $data, true);

			$data['content'] = "";

			if ($this -> input -> post("sale") == "none") {
				$data['content'] = "You have not select the type of report that you want to know.Please select again";
			}
			if ($this -> input -> post("sale") == "customer") {
				redirect('http://localhost/online/index.php/admin/customersale ', 'refresh');
			}
			if ($this -> input -> post("sale") == "total") {
				$data['content'] = $this -> load -> view('salesreport', $data, true);
			}
			if ($this -> input -> post("sale") == "product") {
				redirect('http://localhost/online/index.php/admin/productsale ', 'refresh');
			}
			if ($this -> input -> post("sale") == "yearly") {
				redirect('http://localhost/online/index.php/admin/yearselect ', 'refresh');
			}

			//......//

			$this -> load -> view('adminindex', $data);
		} else {
			if ($this -> session -> userdata("is_loged_in") == false) {
				redirect('http://localhost/online/index.php/main/login', 'refresh');
			}
			if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") != 1) {
				redirect('http://localhost/online/index.php/customer', 'refresh');
			}

		}

	}

	public function yearselect() {
		if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") == 1) {

			$data['base'] = $this -> config -> item('base');
			$data['panel'] = $this -> load -> view('dashbordpanel', $data, true);
			//msg showing the total overviw of the store ..database queries must be needed but not yet implemented..//
			$data['msg'] = $this -> load -> view('yearselect', $data, true);
			if ($this -> input -> post("year") == "") {
				$data['content'] = "Please Select the year..";
			} else {
				$data["report"] = $this -> edata -> getyearlyreport($this -> input -> post("year"));
				$data['content'] = $this -> load -> view('yearlyreport', $data, true);
			}

			//......//

			$this -> load -> view('adminindex', $data);
		} else {
			if ($this -> session -> userdata("is_loged_in") == false) {
				redirect('http://localhost/online/index.php/main/login', 'refresh');
			}
			if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") != 1) {
				redirect('http://localhost/online/index.php/customer', 'refresh');
			}

		}
	}

	public function customersale() {
		if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") == 1) {

			$data['base'] = $this -> config -> item('base');
			$data['panel'] = $this -> load -> view('dashbordpanel', $data, true);
			//msg showing the total overviw of the store ..database queries must be needed but not yet implemented..//
			$data['msg'] = "";
			$config['base_url'] = "http://localhost/online/index.php/admin/customersale/";
			$config['total_rows'] = $this -> db -> get("customerinfo") -> num_rows();
			$config['per_page'] = '2';
			$config['full_tag_open'] = '  <div class="pagination">  ';
			$config['full_tag_close'] = '  </div>';

			$this -> pagination -> initialize($config);
			///.....////
			$data["report"] = $this -> edata -> salesreportcustomer($config['per_page']);

			$data['content'] = $this -> load -> view('salesreportcustomer', $data, true);
			//......//

			$this -> load -> view('adminindex', $data);
		} else {
			if ($this -> session -> userdata("is_loged_in") == false) {
				redirect('http://localhost/online/index.php/main/login', 'refresh');
			}
			if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") != 1) {
				redirect('http://localhost/online/index.php/customer', 'refresh');
			}

		}
	}

	public function detailscustomer($id = "") {
		if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") == 1) {

			$data['base'] = $this -> config -> item('base');
			$data['panel'] = $this -> load -> view('dashbordpanel', $data, true);
			//Database //
			$data["customer"] = $this -> edata -> customerdetails($id);
			//Database//
			$data['msg'] = "";
			$data['content'] = $this -> load -> view('customerdetails', $data, true);
			//......//

			$this -> load -> view('adminindex', $data);
		} else {
			if ($this -> session -> userdata("is_loged_in") == false) {
				redirect('http://localhost/online/index.php/main/login', 'refresh');
			}
			if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") != 1) {
				redirect('http://localhost/online/index.php/customer', 'refresh');
			}

		}
	}

	public function productsale() {
		if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") == 1) {

			$data['base'] = $this -> config -> item('base');
			$data['panel'] = $this -> load -> view('dashbordpanel', $data, true);
			//msg showing the total overviw of the store ..database queries must be needed but not yet implemented..//
			$data['msg'] = "";
			$config['base_url'] = "http://localhost/online/index.php/admin/productsale/";
			$config['total_rows'] = $this -> db -> get("productinfo") -> num_rows();
			$config['per_page'] = '2';
			$config['full_tag_open'] = '  <div class="pagination">  ';
			$config['full_tag_close'] = '  </div>';

			$this -> pagination -> initialize($config);
			///.....////
			$data["report"] = $this -> edata -> salesreportproduct($config['per_page']);

			$data['content'] = $this -> load -> view('productsalesreport', $data, true);
			//......//

			$this -> load -> view('adminindex', $data);
		} else {
			if ($this -> session -> userdata("is_loged_in") == false) {
				redirect('http://localhost/online/index.php/main/login', 'refresh');
			}
			if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") != 1) {
				redirect('http://localhost/online/index.php/customer', 'refresh');
			}

		}
	}

	public function productdetails($id = "") {
		if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") == 1) {

			$data['base'] = $this -> config -> item('base');
			$data['panel'] =  $this -> load -> view('dashbordpanel', $data, true);
			//Database //
			$data["product"] = $this -> edata -> productdetails($id);
			//Database//
			$data['msg'] = "";
			$data['content'] = $this -> load -> view('productdetsalereport', $data, true);
			//......//

			$this -> load -> view('adminindex', $data);
		} else {
			if ($this -> session -> userdata("is_loged_in") == false) {
				redirect('http://localhost/online/index.php/main/login', 'refresh');
			}
			if ($this -> session -> userdata("is_loged_in") == true && $this -> session -> userdata("admin") != 1) {
				redirect('http://localhost/online/index.php/customer', 'refresh');
			}

		}
	}

	public function logout() {
		$this -> session -> sess_destroy();

		redirect('http://localhost/online/index.php/main', 'refresh');
	}

}
?>