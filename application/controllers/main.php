<?php
/**
 * @author Shovon
 * @copyright 2012
 * Main class ..this class will be called at the first time ...based on the login User will be redirecter to
 * another class ...
 */

class Main extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this -> load -> model('edata');
		$this -> load -> helper('form');
		$this -> load -> library('session');
		$this -> load -> helper('url');
	}

	public function index() {

		$data['base'] = $this -> config -> item('base');
		$data['base_url'] = $this -> config -> item('base_url');
		$data['images'] = $this -> config -> item('img_url');

		$data['cart'] = "Welcome Visitor <br/> Today is :" . date('l \t\h\e jS');
		$data['main'] = $this -> load -> view('slider', $data, true);
		$data['body'] = $this -> load -> view('productsample', $data, true);
		$data['sidebar'] = $this -> load -> view('search', $data, true);
		$data['menu'] = $this -> load -> view('catagory', $data, true);
		$data['sidefull'] = $this -> load -> view('moreproduct', $data, true);
		$data['last'] = $this -> load -> view('cols', $data, true);
		$data['foot'] = $this -> load -> view('foot', $data, true);
		$this -> load -> view('index', $data);
	}

	public function login() {

		$data['base'] = $this -> config -> item('base');
		$data['base_url'] = $this -> config -> item('base_url');
		$data['images'] = $this -> config -> item('img_url');

		// 3 ta parametre na pthaile hoy na load ..//
		$data['cart'] = "Welcome Visitor";
		$data['main'] = $this -> load -> view('login', $data, true);
		$data['body'] = "";
		$data['sidebar'] = $this -> load -> view('search', $data, true);
		$data['menu'] = $this -> load -> view('catagory', $data, true);
		$data['sidefull'] = $this -> load -> view('moreproduct', $data, true);
		$data['last'] = $this -> load -> view('cols', $data, true);
		$data['foot'] = $this -> load -> view('foot', $data, true);

		//load view ...//
		$this -> load -> view('index', $data);

	}

	public function verifylogin() {

		$data['base'] = $this -> config -> item('base_url');
		$data['images'] = $this -> config -> item('img_url');

		// 3 ta parametre na pthaile hoy na load ..//
		$data['cart'] = "Welcome Visitor";

		$data['body'] = $this -> load -> view('productsample', $data, true);
		$data['sidebar'] = $this -> load -> view('search', $data, true);
		$data['menu'] = $this -> load -> view('catagory', $data, true);
		$data['sidefull'] = $this -> load -> view('moreproduct', $data, true);
		$data['last'] = $this -> load -> view('cols', $data, true);
		$data['foot'] = $this -> load -> view('foot', $data, true);

		if ($this -> edata -> login()) {
			if ($this -> session -> userdata('admin') == NULL) {
				redirect('customer/index', 'refresh');
			} else {
				redirect('admin/index', 'refresh');
			}

		} else {
			$data['main'] = "Sorry you are not Registerd";
		}

		//load view ...//
		$this -> load -> view('index', $data);
	}

	public function delivery() {

		$data['base'] = $this -> config -> item('base');
		$data['base_url'] = $this -> config -> item('base_url');
		$data['images'] = $this -> config -> item('img_url');
		/// database query...//
		$data['deliery'] = $this -> edata -> delivery();
		//.....//
		$data['cart'] = "Welcome Visitor <br/> Today is :" . date('l \t\h\e jS');
		$data['main'] = $this -> load -> view('d_areas', $data, true);
		$data['body'] = "";
		$data['sidebar'] = $this -> load -> view('search', $data, true);
		$data['menu'] = $this -> load -> view('catagory', $data, true);
		$data['sidefull'] = $this -> load -> view('moreproduct', $data, true);
		$data['last'] = $this -> load -> view('cols', $data, true);
		$data['foot'] = $this -> load -> view('foot', $data, true);
		$this -> load -> view('index', $data);
	}

	public function showsuggest() {

		$data['base'] = $this -> config -> item('base');
		$data['base_url'] = $this -> config -> item('base_url');
		$data['images'] = $this -> config -> item('img_url');
		/// database query...//

		//.....//
		$data['cart'] = "Welcome Visitor <br/> Today is :" . date('l \t\h\e jS');
		$data['main'] = $this -> load -> view('suggest', $data, true);
		$data['body'] = "";
		$data['sidebar'] = $this -> load -> view('search', $data, true);
		$data['menu'] = $this -> load -> view('catagory', $data, true);
		$data['sidefull'] = $this -> load -> view('moreproduct', $data, true);
		$data['last'] = $this -> load -> view('cols', $data, true);
		$data['foot'] = $this -> load -> view('foot', $data, true);
		$this -> load -> view('index', $data);

	}

	public function insertsuggest() {

		/// database query...//
		$data['name'] = $this -> input -> post("name");
		$data['email'] = $this -> input -> post("email");
		$data['phone'] = $this -> input -> post("phone");
		$data['date'] = date("Y-m-d");
		$data['message'] = $this -> input -> post("detail");

		$success = $this -> edata -> suggest($data);
		//.....//

		$data['base'] = $this -> config -> item('base');
		$data['base_url'] = $this -> config -> item('base_url');
		$data['images'] = $this -> config -> item('img_url');
		$data['cart'] = "Welcome Visitor <br/> Today is :" . date('l \t\h\e jS');
		$data['main'] = "Succesfully Send";
		$data['body'] = "";
		$data['sidebar'] = $this -> load -> view('search', $data, true);
		$data['menu'] = $this -> load -> view('catagory', $data, true);
		$data['sidefull'] = $this -> load -> view('moreproduct', $data, true);
		$data['last'] = $this -> load -> view('cols', $data, true);
		$data['foot'] = $this -> load -> view('foot', $data, true);
		$this -> load -> view('index', $data);
	}

	public function showform() {
		$data['base'] = $this -> config -> item('base');
		$data['base_url'] = $this -> config -> item('base_url');
		$data['images'] = $this -> config -> item('img_url');
		$data['cart'] = "Welcome Visitor <br/> Today is :" . date('l \t\h\e jS');
		$data['main'] = $this -> load -> view('registryform', $data, true);
		$data['body'] = "";
		$data['sidebar'] = $this -> load -> view('search', $data, true);
		$data['menu'] = $this -> load -> view('catagory', $data, true);
		$data['sidefull'] = $this -> load -> view('moreproduct', $data, true);
		$data['last'] = $this -> load -> view('cols', $data, true);
		$data['foot'] = $this -> load -> view('foot', $data, true);
		$this -> load -> view('index', $data);
	}

	public function customerinsert() {
		$data['name'] = $this -> input -> post("name");
		$data['country'] = $this -> input -> post("country");
		$data['state'] = $this -> input -> post("state");
		$data["address"] = $this -> input -> post("add");
		$data['contact_no'] = $this -> input -> post("contact_no");
		$data['email'] = $this -> input -> post("email");
		$data['password'] = $this -> input -> post("password");
		$data['account_num'] = 0;
		$data['total_credit'] = 0;
		$data['admin'] = 0;
		$data['date'] = date("Y-m-d");

		$success = $this -> edata -> insertcustomer($data);
		if ($success) {
			$data['base'] = $this -> config -> item('base');
			$data['base_url'] = $this -> config -> item('base_url');
			$data['images'] = $this -> config -> item('img_url');
				$data['cart'] = "Welcome Visitor <br/> Today is :" . date('l \t\h\e jS');
		
			$data['main'] = "You have Succesfully Registered ..Please Login to enter your account";
			$data['body'] = "";
			$data['sidebar'] = $this -> load -> view('search', $data, true);
			$data['menu'] = $this -> load -> view('catagory', $data, true);
			$data['sidefull'] = $this -> load -> view('moreproduct', $data, true);
			$data['last'] = $this -> load -> view('cols', $data, true);
			$data['foot'] = $this -> load -> view('foot', $data, true);
			$this -> load -> view('index', $data);
		} else {
			$data['base'] = $this -> config -> item('base');
			$data['base_url'] = $this -> config -> item('base_url');
			$data['images'] = $this -> config -> item('img_url');
			$data['cart'] = "Welcome Visitor <br/> Today is :" . date('l \t\h\e jS');
			$data['main'] = "Sorry error Occured ";
			$data['body'] = "";
			$data['sidebar'] = $this -> load -> view('search', $data, true);
			$data['menu'] = $this -> load -> view('catagory', $data, true);
			$data['sidefull'] = $this -> load -> view('moreproduct', $data, true);
			$data['last'] = $this -> load -> view('cols', $data, true);
			$data['foot'] = $this -> load -> view('foot', $data, true);
			$this -> load -> view('index', $data);
		}

	}

	public function product($catid = "") {
		$data['base'] = $this -> config -> item('base');
		$data['base_url'] = $this -> config -> item('base_url');
		$data['images'] = $this -> config -> item('img_url');
		/// database ...//
		$data["subcatagory"] = $this -> edata -> loadcatagory($catid);
		//....//
		if ($data["subcatagory"] -> num_rows > 0) {
			$data["product"] = $data["subcatagory"] -> result();
            $data["cat_id"] = $this -> edata -> parentcatagory($catid);
            	if ($this -> session -> userdata('is_loged_in')){
				$data['cart'] = $this -> load -> view('carticon', $data, true);
			}
			else{
					$data['cart'] = "Welcome Visitor <br/> Today is :" . date('l \t\h\e jS');
			}
		
			$data['main'] = $this -> load -> view('showproduct', $data, true);
			$data['body'] = "";
			$data['sidebar'] = $this -> load -> view('search', $data, true);
			$data['menu'] = $this -> load -> view('catagory', $data, true);
			$data['sidefull'] = $this -> load -> view('moreproduct', $data, true);
			$data['last'] = $this -> load -> view('cols', $data, true);
			$data['foot'] = $this -> load -> view('foot', $data, true);
			$this -> load -> view('index', $data);
		} else {
			/// database ...//
			$data["product"] = $this -> edata -> loadproduct($catid);
			$data["cat_id"] = $this -> edata -> parentcatagory($catid);
			//....//
				if ($this -> session -> userdata('is_loged_in')){
				$data['cart'] = $this -> load -> view('carticon', $data, true);
			}
			else{
					$data['cart'] = "Welcome Visitor <br/> Today is :" . date('l \t\h\e jS');
			}
		
			$data['main'] = $this -> load -> view('viewproduct', $data, true);
			$data['body'] = "";
			$data['sidebar'] = $this -> load -> view('search', $data, true);
			$data['menu'] = $this -> load -> view('catagory', $data, true);
			$data['sidefull'] = $this -> load -> view('moreproduct', $data, true);
			$data['last'] = $this -> load -> view('cols', $data, true);
			$data['foot'] = $this -> load -> view('foot', $data, true);
			$this -> load -> view('index', $data);
		}

	}

	public function details($id = "") {

		$data['base'] = $this -> config -> item('base');
		$data['base_url'] = $this -> config -> item('base_url');
		$data['images'] = $this -> config -> item('img_url');
		/// database ...///
		$data["product"] = $this -> edata -> productdetails($id);
		////....///
			if ($this -> session -> userdata('is_loged_in')){
				$data['cart'] = $this -> load -> view('carticon', $data, true);
			}
			else{
					$data['cart'] = "Welcome Visitor <br/> Today is :" . date('l \t\h\e jS');
			}
	
		$data['main'] = $this -> load -> view('productdetails', $data, true);
		$data['body'] = "";
		$data['sidebar'] = $this -> load -> view('search', $data, true);
		$data['menu'] = $this -> load -> view('catagory', $data, true);
		$data['sidefull'] = $this -> load -> view('moreproduct', $data, true);
		$data['last'] = $this -> load -> view('cols', $data, true);
		$data['foot'] = $this -> load -> view('foot', $data, true);
		$this -> load -> view('index', $data);
	}

}
?>