-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 01, 2012 at 11:49 PM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `eshop`
--

-- --------------------------------------------------------

--
-- Table structure for table `buy_records`
--

CREATE TABLE IF NOT EXISTS `buy_records` (
  `product_id` int(5) NOT NULL,
  `bought_quantity` int(11) NOT NULL,
  `bought_price` float NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `buy_records`
--

INSERT INTO `buy_records` (`product_id`, `bought_quantity`, `bought_price`, `date`) VALUES
(1000, 30, 120, '2012-04-01'),
(1001, 20, 1500, '2012-04-02'),
(1002, 20, 2500, '2012-04-29'),
(1003, 15, 3000, '2012-04-29'),
(1004, 25, 2000, '2012-04-29'),
(1008, 20, 3000, '2012-04-30'),
(1009, 15, 1000, '2012-05-01');

-- --------------------------------------------------------

--
-- Table structure for table `cancel_list`
--

CREATE TABLE IF NOT EXISTS `cancel_list` (
  `customer_id` int(5) NOT NULL,
  `cancelled_order` int(11) NOT NULL,
  `amount` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cancel_list`
--

INSERT INTO `cancel_list` (`customer_id`, `cancelled_order`, `amount`) VALUES
(11111, 55555, 6860),
(22222, 66666, 811210);

-- --------------------------------------------------------

--
-- Table structure for table `cart_table`
--

CREATE TABLE IF NOT EXISTS `cart_table` (
  `customer_id` int(5) NOT NULL,
  `product_id` int(5) NOT NULL,
  `product_name` text NOT NULL,
  `unit_price` float NOT NULL,
  `avg_bought_price` float NOT NULL,
  `quantity` int(11) NOT NULL,
  `discount` float NOT NULL,
  `total` int(11) NOT NULL,
  `memo` float NOT NULL COMMENT 'total - quantity * avg_bought_price'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `catagory`
--

CREATE TABLE IF NOT EXISTS `catagory` (
  `cat_id` int(5) NOT NULL AUTO_INCREMENT,
  `cat_name` text NOT NULL,
  `image` text NOT NULL,
  `parent_cat` int(11) NOT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10008 ;

--
-- Dumping data for table `catagory`
--

INSERT INTO `catagory` (`cat_id`, `cat_name`, `image`, `parent_cat`) VALUES
(10000, 'Clothings', '', 0),
(10001, 'Men''s Wear', 'http://localhost/online/img/shirt.jpg', 10000),
(10002, 'Women''s Wear', 'http://localhost/online/img/shari.jpg', 10000),
(10003, 'Winter Jacket(Man''s)', 'http://localhost/online/img/jacket8.jpg', 10000),
(10004, 'Winter Jacket(Woman''s)', 'http://localhost/online/img/jacket5.jpg', 10000),
(10005, 'Panjabi', 'http://localhost/online/img/panjabi.jpg', 10000),
(10006, 'Shoes', 'http://localhost/online/img/TOMS_shoes.jpg', 0),
(10007, 'Men''s Shoe', 'http://localhost/online/img/nike-shoes-fl-2.jpg', 10006);

-- --------------------------------------------------------

--
-- Table structure for table `customerinfo`
--

CREATE TABLE IF NOT EXISTS `customerinfo` (
  `customer_id` int(5) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `country` text NOT NULL,
  `state` text NOT NULL,
  `address` text NOT NULL,
  `contact_no` int(11) NOT NULL,
  `email` text NOT NULL,
  `password` varchar(20) NOT NULL,
  `account_num` int(11) NOT NULL,
  `total_credit` float NOT NULL,
  `admin` int(11) NOT NULL,
  `date` date NOT NULL,
  `num_of_purchase` int(11) NOT NULL,
  `total_product_purchased` int(11) NOT NULL,
  `purchase_amount` float NOT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12116 ;

--
-- Dumping data for table `customerinfo`
--

INSERT INTO `customerinfo` (`customer_id`, `name`, `country`, `state`, `address`, `contact_no`, `email`, `password`, `account_num`, `total_credit`, `admin`, `date`, `num_of_purchase`, `total_product_purchased`, `purchase_amount`) VALUES
(11111, 'Rifatul Islam ', 'Bangladesh', 'Dhaka', '2202,dhaka,kollanpur', 234234234, 'shovonis@yahoo.com', '11111', 0, 0, 1, '2012-01-01', 0, 0, 0),
(11112, 'Umama Tasnim ', 'Bangladesh', 'Dhaka', 'Dholpur,Jatrabari', 234234, 'mousumitasnim@gmail.com', '22222', 0, 0, 1, '2012-01-01', 0, 0, 0),
(11122, 'Mr.Jodu ', 'Bangladesh', 'Dhaka', 'Farmgate,Dhaka', 12038208, 'jodu@uradhura.com', '33333', 55555, 7000, 0, '2012-04-01', 1, 3, 3000),
(11123, 'Showmic Islam Ratul ', 'Bangladesh', 'Dhaka', 'Ganda,Savar,Dhaka', 13244545, 'showmic@yahoo.com', '44444', 88888, 8500, 0, '2012-03-05', 8, 12, 11750),
(12111, 'Shovon', 'Bangladesh', 'Dhaka', '22/1 Indira Road,Farmgate,Dhaka', 5465465, 'a@yahoo.com', '99999', 66666, 5500, 0, '2012-04-15', 3, 10, 11500),
(12112, 'Karim', 'Bangladesh', 'Dhaka', '22,1 Paikpara,Dhaka', 44234, 'kam@yahoo.com', '99999', 0, 0, 0, '2012-04-22', 0, 0, 0),
(12113, 'Rahim Ullah ', 'Bangladesh', 'Dhaka', 'Farmgate,Dhaka', 455454, 'ram@yahoo.com', '88888', 0, 0, 0, '2012-04-22', 0, 0, 0),
(12114, 'Shihab Rahman', 'Bangladesh', 'Dhaka', 'Dhanmondi,Dhaka', 904934093, 'shihab@gmail.com', '00000', 0, 0, 0, '2012-04-30', 0, 0, 0),
(12115, 'XYZ', 'Bangladesh', 'Dhaka', 'Jatrabari,Dhaka', 34234234, 'xyz@yahoo.com', '44444', 88888, 9992250, 0, '2012-05-01', 1, 8, 7750);

-- --------------------------------------------------------

--
-- Table structure for table `d_areas`
--

CREATE TABLE IF NOT EXISTS `d_areas` (
  `country` varchar(20) NOT NULL,
  `areas` text NOT NULL,
  `shipment_charge` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `d_areas`
--

INSERT INTO `d_areas` (`country`, `areas`, `shipment_charge`) VALUES
('Bangladesh', 'Dhaka', 50),
('Bangladesh', 'Chittagong', 100),
('Bangladesh', 'Khulna', 100),
('Bangladesh', 'Barishal', 100),
('Bangladesh', 'Rajshahi', 100);

-- --------------------------------------------------------

--
-- Table structure for table `inventory`
--

CREATE TABLE IF NOT EXISTS `inventory` (
  `product_id` int(11) NOT NULL,
  `product_name` text NOT NULL,
  `avg_bought_price` float NOT NULL,
  `stock` int(11) NOT NULL,
  `threshold` int(11) NOT NULL,
  `selling_price` float NOT NULL,
  `num_sale` int(11) NOT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inventory`
--

INSERT INTO `inventory` (`product_id`, `product_name`, `avg_bought_price`, `stock`, `threshold`, `selling_price`, `num_sale`) VALUES
(1000, 't-shirt', 131, 25, 10, 250, 22),
(1001, 'shari', 1500, 13, 5, 2500, 7),
(1002, 'Woolen Jacket', 2500, 20, 5, 5000, 0),
(1003, 'Long Jacket', 3000, 15, 5, 7000, 0),
(1004, 'Woman''s Jacket 1', 2000, 25, 5, 4500, 0),
(1008, 'Jamdani Shari', 3000, 19, 5, 5000, 1),
(1009, 'Blue Panjabi', 1000, 12, 5, 2000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE IF NOT EXISTS `message` (
  `customer_id` int(11) NOT NULL,
  `name` text NOT NULL,
  `email` text NOT NULL,
  `phone` int(11) NOT NULL,
  `date` date NOT NULL,
  `message` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`customer_id`, `name`, `email`, `phone`, `date`, `message`) VALUES
(0, 'Rifatul', 'shovon_09@hotmail.com', 34534534, '2012-04-10', 'Nothing '),
(0, 'Mousumi Tasnim', 'mousumitasnim@gmail.com', 767678687, '2012-04-10', 'Something'),
(0, '', '', 0, '2012-04-22', '');

-- --------------------------------------------------------

--
-- Table structure for table `newsletter`
--

CREATE TABLE IF NOT EXISTS `newsletter` (
  `date` date NOT NULL,
  `news` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `prepaid`
--

CREATE TABLE IF NOT EXISTS `prepaid` (
  `pin` int(11) NOT NULL AUTO_INCREMENT,
  `amount` int(11) NOT NULL,
  `used` int(11) NOT NULL,
  PRIMARY KEY (`pin`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `productinfo`
--

CREATE TABLE IF NOT EXISTS `productinfo` (
  `product_id` int(5) NOT NULL AUTO_INCREMENT,
  `product_name` text NOT NULL,
  `description` text NOT NULL,
  `image` text NOT NULL,
  `cat_id` int(5) NOT NULL,
  `date_added` date NOT NULL,
  `product_type` text NOT NULL,
  `promotion` float NOT NULL,
  `promo_from` date DEFAULT NULL,
  `promo_to` date DEFAULT NULL,
  `promo_active` int(11) NOT NULL,
  PRIMARY KEY (`product_id`),
  KEY `cat_id` (`cat_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1010 ;

--
-- Dumping data for table `productinfo`
--

INSERT INTO `productinfo` (`product_id`, `product_name`, `description`, `image`, `cat_id`, `date_added`, `product_type`, `promotion`, `promo_from`, `promo_to`, `promo_active`) VALUES
(1000, 't-shirt', 'blah...blah...', 'http://localhost/online/img/tshirt.jpg', 10001, '2012-04-01', 'T-shirt', 0, '0000-00-00', '0000-00-00', 0),
(1001, 'shari', 'blah ..blah..', 'http://localhost/online/img/jamdani.jpg	', 10002, '2012-04-02', 'shari', 0, '0000-00-00', '0000-00-00', 0),
(1002, 'Woolen Jacket', 'Blah ..blah ...', 'http://localhost/online/img/jacket2.jpg', 10003, '2012-04-29', 'Jacket', 0, '0000-00-00', '0000-00-00', 0),
(1003, 'Long Jacket', 'Blah ...bLah ...', 'http://localhost/online/img/jacket3.jpg', 10003, '2012-04-29', 'Jacket', 10, '2012-04-29', '2012-08-31', 1),
(1004, 'Woman''s jacket 1 ', 'Blah ..Blah ..', 'http://localhost/online/img/jacket7.jpg', 10004, '2012-04-29', 'Jacket', 0, NULL, NULL, 0),
(1008, 'Jamdani Shari', 'Red Jamdani Shari ', 'http://localhost/online/img/shari2.jpg', 10002, '2012-04-30', 'Shari', 0, '0000-00-00', '0000-00-00', 0),
(1009, 'Blue Panjabi', 'blah ...blah...', 'http://localhost/online/img/panjabi1.jpg', 10005, '2012-05-01', 'panjabi', 0, '0000-00-00', '0000-00-00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `purchase`
--

CREATE TABLE IF NOT EXISTS `purchase` (
  `purchase_id` int(5) NOT NULL AUTO_INCREMENT,
  `customer_id` int(5) NOT NULL,
  `delivery_div` text NOT NULL,
  `delivery_address` text NOT NULL,
  `date` date NOT NULL,
  `pin` int(5) NOT NULL,
  `shipment_charge` float NOT NULL,
  `total_quantity` int(11) NOT NULL,
  `sales` float NOT NULL,
  `sum_avg_b_price` float NOT NULL,
  `profit` float NOT NULL,
  PRIMARY KEY (`purchase_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10020 ;

--
-- Dumping data for table `purchase`
--

INSERT INTO `purchase` (`purchase_id`, `customer_id`, `delivery_div`, `delivery_address`, `date`, `pin`, `shipment_charge`, `total_quantity`, `sales`, `sum_avg_b_price`, `profit`) VALUES
(10000, 11122, 'Bangladesh', 'Dhaka', '2012-01-02', 55555, 20, 3, 3000, 1740, 1260),
(10001, 12111, 'Bangladesh', 'Dhaka', '2012-04-12', 66666, 50, 3, 5250, 3120, 2130),
(10002, 12111, 'Bangladesh', 'Mirpur-1,Dhaka', '2012-04-19', 55555, 30, 1, 250, 120, 130),
(10007, 11123, ' Bangladesh ', '222,Indira,Road', '2012-04-27', 88888, 100, 2, 2750, 1620, 1130),
(10008, 11123, ' Bangladesh ', '', '2012-04-27', 88888, 100, 2, 500, 240, 260),
(10009, 11123, ' Bangladesh ', 'Dholpur,Jatrabari,Dhaka', '2012-04-27', 88888, 100, 2, 500, 240, 260),
(10010, 12111, ' Bangladesh ', 'chitagongroad.', '2012-04-27', 66666, 100, 6, 6000, 3480, 2520),
(10011, 11123, ' Bangladesh ', '222,Indira,Road', '2012-04-27', 88888, 100, 1, 250, 120, 130),
(10012, 11123, ' Bangladesh ', '222,Indira,Road', '2012-04-27', 88888, 100, 1, 250, 120, 130),
(10013, 11123, ' Bangladesh ', 'chitagongroad.', '2012-04-27', 88888, 100, 2, 500, 240, 260),
(10015, 11123, ' Bangladesh ', 'Ganda,Savar,Dhaka', '2012-04-30', 88888, 100, 1, 5000, 3000, 2000),
(10018, 11123, ' Bangladesh ', '', '2012-05-01', 88888, 100, 1, 2000, 1000, 1000),
(10019, 12115, ' Bangladesh ', 'Dholpur,Jatrabari,Dhaka', '2012-05-01', 88888, 100, 8, 7750, 4155, 3595);

-- --------------------------------------------------------

--
-- Table structure for table `voucher`
--

CREATE TABLE IF NOT EXISTS `voucher` (
  `purchase_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `quantity` int(11) NOT NULL,
  `selling_price` float NOT NULL,
  `avg_bought_price` float NOT NULL,
  `memo` float NOT NULL COMMENT '(selling price-avg_bought price)*quantity'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `voucher`
--

INSERT INTO `voucher` (`purchase_id`, `product_id`, `date`, `quantity`, `selling_price`, `avg_bought_price`, `memo`) VALUES
(10000, 1000, '2012-01-02', 2, 250, 120, 260),
(10000, 1001, '2012-01-02', 1, 2500, 1500, 1000),
(10001, 1000, '2012-04-12', 1, 250, 120, 130),
(10001, 1001, '2012-04-12', 2, 2500, 1500, 2000),
(10002, 1000, '2012-04-19', 1, 250, 120, 130),
(10012, 1000, '0000-00-00', 1, 250, 120, 130),
(10013, 1000, '2012-04-27', 2, 250, 120, 260),
(10015, 1008, '2012-04-30', 1, 5000, 3000, 2000),
(10018, 1009, '2012-05-01', 1, 2000, 1000, 1000),
(10019, 1000, '2012-05-01', 5, 250, 131, 595),
(10019, 1001, '2012-05-01', 1, 2500, 1500, 1000),
(10019, 1009, '2012-05-01', 2, 2000, 1000, 2000);

-- --------------------------------------------------------

--
-- Table structure for table `web`
--

CREATE TABLE IF NOT EXISTS `web` (
  `about` text NOT NULL,
  `terms_cond` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='table for updating  the webpage';

--
-- Dumping data for table `web`
--

INSERT INTO `web` (`about`, `terms_cond`) VALUES
('Online Megashop ', 'Terms and Condition ');

-- --------------------------------------------------------

--
-- Table structure for table `wish_list`
--

CREATE TABLE IF NOT EXISTS `wish_list` (
  `customer_id` int(11) NOT NULL,
  `product_id` int(5) NOT NULL,
  `product_name` text NOT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
